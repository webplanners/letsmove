LetsMove.gr
===========

## Admin Panel Customization

`resources/css/nova-customization.css`

## Admin Panel Translations

`resources/lang/vendor/nova/*.json`
