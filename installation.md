LetsMove.gr Installation
========================

### Setup

1. `git clone https://theofanis_@bitbucket.org/webplanners/letsmove.git` 
1. `composer install`
1. redis 
1. supervisor - horizon.conf
1. crontab `* * * * * cd /home/letsmove/domains/letsmove.gr && php artisan schedule:run >> /dev/null 2>&1`

## Image Handling Requirements [spatie/laravel-medialibrary](https://spatie.be/docs/laravel-medialibrary/v8/requirements)

- [Requirements](https://spatie.be/docs/laravel-medialibrary/v8/requirements)
    1. `sudo apt install -y php7.4-imagick`
    1. `sudo apt install -y php7.4-gd`
    1. `sudo apt install -y ffmpeg` for video thumbnails
    1. `sudo apt install -y ghostscript` for pdf thumbnails
    1. exif (installed by default) `php -m | grep exif `
    
- [Image Optimizers](https://spatie.be/docs/laravel-medialibrary/v8/installation-setup#optimization-tools)
    1. optimizers `sudo apt install -y jpegoptim optipng pngquant gifsicle`
    1. optimizers `sudo npm install -g svgo`

# Stripe

- Webhooks

```
# .env
# 1 ώρες για διαφορα θερινής ώρας Ελλάδας + 5 λεπτά default καθυστέρηση
STRIPE_WEBHOOK_TOLERANCE=4100 
```

### Products

Products must have metadata 
 - `product=broker-listings`
 - `listings=50` πόσες αγγελίες μπορούν να ανεβάσουν, μεγάλος αριθμός για απεριόριστες


# Nginx

```
server {
      client_max_body_size 100M;
}   
```
