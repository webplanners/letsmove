Nova.booting((Vue, router) => {
    if (!Nova.config.stripe.key)
        return;
    Nova.stripe = Stripe(Nova.config.stripe.key);
    router.addRoutes([
        {
            name     : 'stripe-checkout',
            path     : '/stripe/checkout',
            props    : true,
            component: {
                beforeRouteEnter(to, from, next) {
                    console.log(from)
                    next(false);
                    Nova.stripe.redirectToCheckout({
                        sessionId: to.query.id
                    }).then(function (result) {
                        console.log(result);
                        if (result.error.message) {

                        }
                        // If `redirectToCheckout` fails due to a browser or network
                        // error, display the localized error message to your customer
                        // using `result.error.message`.
                    });
                }
            },
        },
    ]);
});
