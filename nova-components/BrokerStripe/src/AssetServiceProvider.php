<?php

namespace Theograms\BrokerStripe;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class AssetServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Nova::serving(function (ServingNova $event) {
            Nova::script('nova-broker-stripe', __DIR__ . '/../dist/js/asset.js');
            Nova::style('nova-broker-stripe', __DIR__ . '/../dist/css/asset.css');
            Nova::style('nova-customization', resource_path('css/nova-customization.css'));
            Nova::provideToScript([
                'stripe' => [
                    'key' => config('cashier.key'),
                ],
            ]);
        });
    }

    public function register()
    {
        //
    }
}
