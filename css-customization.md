HTML/CSS Customization
======================


    resources/views/ 
        welcome.blade.php -- η αρχική γκρι σελίδα 
    
    resources/views/auth/
        register.blade.php
        register-broker.blade.php
        verify-email.blade.php

Τα υπόλοιπα αρχεία σε αυτόν τον φάκελο δε χρησιμοποιούνται.


    resources/views/vendor/nova/
        layout.blade.php εδώ βάλε μόνο css classes στα στοιχεία που θες και να θα τις ορίσεις σε άλλο αρχείο
        partials/
            user.blade.php είναι το dropdown στο όνομα χρήστη
            footer.blade.php
            logo.blade.php
        auth/
            layout.blade.php -- το layout για όλες τις σελίδες σύνδεση, επαναφορά κωδικού κλπ
            login.blade.php
    
    resources/css/
        nova-customization.css -- εδώ ορίζεις τις css κλάσεις κλπ που θα φορτώνονται στο admin panel


#### ΣΗΜΕΙΩΣΗ

Ίσως χρειαστεί να πειράξεις επιπλέον αρχεία τα οποία δεν έχω σημειώσει, αλλά αυτό δεν είναι πρόβλημα.
Στο τέλος πρέπει να μου στείλεις λίστα με **όλα** τα αρχεία που τροποποίησες για να αποθηκεύσω μόνιμα.
