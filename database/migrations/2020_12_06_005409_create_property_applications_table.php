<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyApplicationsTable extends Migration
{
    public function up()
    {
        Schema::create('property_applications', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->float('negotiation_price')->nullable();
            $table->string('message')->nullable();
            $table->foreignId('property_id');
            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreignId('broker_id');
            $table->foreign('broker_id')->references('id')->on('brokers');
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('property_applications');
    }
}
