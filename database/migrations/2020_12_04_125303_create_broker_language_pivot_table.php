<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrokerLanguagePivotTable extends Migration
{
    public function up()
    {
        Schema::create('broker_language', function (Blueprint $table) {
            $table->foreignId('broker_id');
            $table->foreign('broker_id')->references('id')->on('brokers');
            $table->foreignId('language_id');
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    public function down()
    {
        Schema::dropIfExists('broker_language');
    }
}
