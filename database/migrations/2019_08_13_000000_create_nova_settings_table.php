<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovaSettingsTable extends Migration
{
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->string('key')->unique()->primary();
            $table->string('value', 8192)->nullable();
            $table->timestamps();
            $table->blameable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
