<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesUsersPivotTable extends Migration
{
    public function up()
    {
        Schema::create('languages_users', function (Blueprint $table) {
            $table->foreignId('language_id');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->primary(['language_id', 'user_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('languages_users');
    }
}
