<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyAttributesTable extends Migration
{
    public function up()
    {
        Schema::create('property_attributes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code')->index();
            $table->string('type');
            $table->json('options');
            $table->string('group')->nullable();
            $table->string('validation');
            $table->foreignId('property_category_id');
            $table->foreign('property_category_id')->references('id')->on('property_categories');
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
            $table->unique(['property_category_id', 'code']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('property_attributes');
    }
}
