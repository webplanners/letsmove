<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('broker_id');
            $table->foreign('broker_id')->references('id')->on('brokers');
            $table->foreignId('property_category_id');
            $table->foreign('property_category_id')->references('id')->on('property_categories');
            $table->foreignId('district_id');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreignId('municipality_id');
            $table->foreign('municipality_id')->references('id')->on('municipalities');
            $table->foreignId('vicinity_id');
            $table->foreign('vicinity_id')->references('id')->on('vicinities');
            $table->string('youtube')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
