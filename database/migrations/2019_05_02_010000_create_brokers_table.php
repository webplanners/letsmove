<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrokersTable extends Migration
{

    public function up()
    {
        Schema::create('brokers', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50);
            $table->string('status',20);
            $table->string('vin', 11)->nullable()->unique();
            $table->string('commercial_registration_number', 12)->nullable()->unique();
            $table->text('description')->nullable();
            $table->string('type',20);
            $table->string('logo')->nullable();
            $table->string('address');
            $table->string('phone',13);
            $table->string('phone_2',13)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('website',40)->nullable();
            $table->string('facebook_username', 40)->nullable();
            $table->string('instagram_username', 40)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->blameable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('brokers');
    }
}
