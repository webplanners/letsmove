<?php

namespace Database\Seeders;

use App\Models\SubscriptionPlan;
use App\Stripe\StripeManager;
use Illuminate\Database\Seeder;
use Stripe\StripeObject;

class DefaultSubscriptionPlansSeeder extends Seeder
{
    public function run(StripeManager $stripeManager)
    {
        // sync existing subscription plans (if any)
        // this will soft delete archived/deleted subscription plans
        SubscriptionPlan::all()->each(function (SubscriptionPlan $plan) {
            $plan->syncFromStripe();
        });
        // then get all plans from stripe and create only plans that do not already exist
        $stripeManager->retrieveSubscriptionPlans()->each(function (StripeObject $product) use ($stripeManager) {
            if (SubscriptionPlan::whereStripeProductId($product->id)->exists())
                return;
            SubscriptionPlan::newModelInstance(['stripe_product_id' => $product->id])->syncFromStripe();
        });
    }
}
