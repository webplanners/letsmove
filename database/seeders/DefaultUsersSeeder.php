<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class DefaultUsersSeeder extends Seeder
{
    public function run()
    {
        $superAdmin = Role::superAdmin();
        User::system()->roles()->save($superAdmin);
        User::cron()->roles()->save($superAdmin);
        collect([
            ['name' => 'Θεοφάνης Βαρδάτσικος', 'username' => 'theofanis', 'email' => 'vardtheo@gmail.com', 'plain_password' => 'Spitia.1', 'roles' => Role::superAdmin()],
            ['name' => 'Ευάγγελος Χατζηδάκης', 'username' => 'exatzidakis', 'email' => 'exatzidakis@gmail.com', 'plain_password' => '$408$408', 'roles' => Role::admin()],
            ['name' => 'Παναγιώτης Σπυρόπουλος', 'username' => 'panosjseven', 'email' => 'panosjseven@gmail.com', 'plain_password' => '$408$408', 'roles' => Role::admin()],
            ['name' => 'Demo Μεσίτης', 'username' => 'demo-broker', 'email' => 'demo-broker@letsmove.gr', 'plain_password' => '$408$408', 'roles' => Role::broker()],
        ])->each(function ($attrs) {
            $roles = Arr::pull($attrs, 'roles');
            $attrs['email_verified_at'] = now();
            $u = User::create($attrs);
            $u->assignRole($roles);
        });
    }

}
