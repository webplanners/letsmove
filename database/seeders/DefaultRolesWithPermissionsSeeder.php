<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;

class DefaultRolesWithPermissionsSeeder extends Seeder
{
    /**
     * @var Role
     */
    protected $superAdmin;
    /**
     * @var Role
     */
    protected $admin;
    /**
     * @var Role
     */
    protected $broker;

    public function __construct()
    {
        $this->superAdmin = Role::superAdmin();
        $this->admin = Role::admin();
        $this->broker = Role::broker();
    }

    public function run()
    {
        $this->setSuperAdminPermissions();
        $this->setAdminPermissions();
        $this->setBrokerPermissions();
        Cache::tags(['can'])->flush();
    }

    protected function setSuperAdminPermissions()
    {
        $this->superAdmin->syncPermissions(Permission::withoutGlobalScopes()->get());
    }

    protected function setAdminPermissions()
    {
        $permissions_map = [
            'admin',
            'server-admin',
            'District'            => '*',
            'Municipality'        => '*',
            'Vicinity'            => '*',
            'Broker'              => '*',
            'Property'            => '*',
            'PropertyApplication' => [
                'viewAny',
                'view',
                'delete',
                'restore',
            ],
            'PropertyCategory'    => '*',
//            'PropertyAttribute'   => '*',//PAID
            'Subscription'        => [
                'viewAny',
                'view',
            ],
            'SubscriptionItem'    => [
                'viewAny',
                'view',
            ],
            'Language'            => '*',
            'User'                => '*',
            'viewAny'             => [
                'Role',
                'Permission',
                'SubscriptionPlan',
            ],
            'view'                => [
                'Role',
                'Permission',
                'SubscriptionPlan',
            ],
        ];
        $this->admin->syncPermissions(flatten_permissions_map($permissions_map));
    }

    protected function setBrokerPermissions()
    {
        $permissions_map = [
            'broker',
            'Broker'              => [
            ],
            'Property'            => [
                'viewAny',
            ],
            'PropertyApplication' => [
                'viewAny',
                'delete',
                'restore',
            ],
            'viewAny'             => [
                'User',
                'District',
                'Municipality',
                'Vicinity',
                'Broker',
                'PropertyCategory',
                'SubscriptionPlan',
                'Subscription',
                'Language',
            ],
            'view'                => [
                'District',
                'Municipality',
                'Vicinity',
                'Broker',
                'PropertyCategory',
                'SubscriptionPlan',
                'Subscription',
                'Language',
            ],
        ];
        $this->broker->syncPermissions(flatten_permissions_map($permissions_map));
    }

}
