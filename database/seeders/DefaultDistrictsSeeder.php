<?php

namespace Database\Seeders;

use App\Imports\DistrictsImport;
use Illuminate\Database\Seeder;

class DefaultDistrictsSeeder extends Seeder
{
    public function run()
    {
        (new DistrictsImport())->import(dirname(__DIR__) . '/districts.csv');
    }
}
