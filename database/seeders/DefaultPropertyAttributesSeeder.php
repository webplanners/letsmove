<?php

namespace Database\Seeders;

use App\Models\PropertyAttribute;
use App\Models\PropertyCategory;
use App\Support\PropertyAttributes;
use Illuminate\Database\Seeder;

class DefaultPropertyAttributesSeeder extends Seeder
{
    public function run()
    {
        foreach (PropertyAttributes::parse() as $category => $data) {
            $pc = PropertyCategory::withoutGlobalScopes()->firstOrCreate(['code' => $category], ['name' => humanize_attr($category)]);
            foreach ($data['attributes'] as $attrs) {
                $attrs['property_category_id'] = $pc->id;
                PropertyAttribute::withoutGlobalScopes()->firstOrCreate(array_only($attrs, ['code', 'property_category_id']), $attrs);
            }
        }
        PropertyAttributes::migrate();
    }
}
