<?php

namespace Database\Seeders;

use App\Imports\MunicipalitiesImport;
use Illuminate\Database\Seeder;

class DefaultMunicipalitiesSeeder extends Seeder
{
    public function run()
    {
        (new MunicipalitiesImport())->import(dirname(__DIR__) . '/municipalities.csv');
    }
}
