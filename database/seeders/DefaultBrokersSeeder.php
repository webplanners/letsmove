<?php

namespace Database\Seeders;

use App\Models\Broker;
use App\Models\User;
use Illuminate\Database\Seeder;

class DefaultBrokersSeeder extends Seeder
{
    public function run()
    {
        collect([
            ['name' => 'Demo Μεσιτικό Γραφείο', 'phone' => 1234567890, 'type' => 'broker', 'address' => 'demo address', 'users' => ['demo-broker', 'theofanis']],
        ])->each(function (array $attrs) {
            $users = array_wrap(array_pull($attrs, 'users'));
            $users = collect($users)->map(function ($key) {
                return User::orWhere(['id' => $key, 'username' => $key, 'email' => $key])->firstOrFail();
            })->filter()->unique('id');
            $b = Broker::create($attrs);
            $users->first()->createToken('testing');
            $b->users()->saveMany($users);
        });
    }
}
