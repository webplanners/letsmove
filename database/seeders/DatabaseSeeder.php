<?php

namespace Database\Seeders;

use App\Models\Setting;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(SystemUserSeeder::class);
        Auth::loginUsingId(User::system()->id);
        $this->call(DefaultRolesSeeder::class);
        $this->call(DefaultPermissionsSeeder::class);
        $this->call(DefaultRolesWithPermissionsSeeder::class);
        $this->call(DefaultUsersSeeder::class);
        $this->call(DefaultLanguagesSeeder::class);
        $this->call(DefaultSubscriptionPlansSeeder::class);
        $this->call(DefaultPropertyAttributesSeeder::class);
        $this->call(DefaultDistrictsSeeder::class);
        $this->call(DefaultMunicipalitiesSeeder::class);
        $this->call(DefaultVicinitiesSeeder::class);
        $this->call(DefaultBrokersSeeder::class);
        Setting::firstOrCreate(['key' => 'app.install_date'], ['value' => now()]);
        Auth::logout();
    }
}
