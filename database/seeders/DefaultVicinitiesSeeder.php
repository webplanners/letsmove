<?php

namespace Database\Seeders;

use App\Imports\VicinitiesImport;
use Illuminate\Database\Seeder;

class DefaultVicinitiesSeeder extends Seeder
{
    public function run()
    {
        (new VicinitiesImport())->import(dirname(__DIR__) . '/vicinities.csv');
    }
}
