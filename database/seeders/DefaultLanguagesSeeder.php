<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class DefaultLanguagesSeeder extends Seeder
{
    public function run()
    {
        collect([
            ['name' => 'English', 'code' => 'en', 'default' => true],
            ['name' => 'Greek', 'code' => 'el', 'default' => true],
            ['name' => 'German', 'code' => 'de'],
            ['name' => 'Arabic', 'code' => 'ar'],
            ['name' => 'Chinese', 'code' => 'zh'],
            ['name' => 'French', 'code' => 'fr'],
            ['name' => 'Turkish', 'code' => 'tr'],
            ['name' => 'Spanish', 'code' => 'es'],
            ['name' => 'Indian', 'code' => 'hi'],
            ['name' => 'Israel', 'code' => 'il'],
            ['name' => 'Italian', 'code' => 'it'],
            ['name' => 'Russian', 'code' => 'ru'],
        ])->each(function ($attrs) {
            $l = Language::firstOrCreate(array_only($attrs, 'code'), $attrs);
        });
    }
}
