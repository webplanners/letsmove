<?php

use App\Http\Controllers\Auth\RegisteredBrokerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

//Route::view('/dashboard', 'dashboard')->middleware(['auth'])->name('dashboard');

Route::get('/register/broker', [RegisteredBrokerController::class, 'create'])->middleware('auth')->name('register-broker');
Route::post('/register/broker', [RegisteredBrokerController::class, 'store'])->middleware('auth');

require __DIR__ . '/auth.php';
