<?php

use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/


Artisan::command('clear:caches', function () {
    $this->call('clear-compiled');
    $this->call('cache:clear');
    $this->call('config:clear');
    $this->call('route:clear');
    $this->call('view:clear');
    $this->call('permission:cache-reset');
})->describe('Clear all application caches');

Artisan::command('ide-helper:all', function () {
    rescue(function () {
        $this->call('ide-helper:eloquent');
        $this->call('ide-helper:model', ['--reset' => true, '--write' => true]);
        $this->call('ide-helper:generate');
        $this->call('ide-helper:meta');
    });
})->describe('Call all ide-helper functions.');

Artisan::command('db:seed:permissions', function () {
    $this->call('db:seed', ['--class' => 'DefaultPermissionsSeeder', '--force' => true]);
    $this->call('cache:clear');
    $this->call('optimize');
})->describe('ReSeed permissions');

Artisan::command('db:seed:roles-permissions', function () {
    $this->call('db:seed', ['--class' => 'DefaultRolesSeeder', '--force' => true]);
    $this->call('db:seed', ['--class' => 'DefaultPermissionsSeeder', '--force' => true]);
    $this->call('db:seed', ['--class' => 'DefaultRolesWithPermissionsSeeder', '--force' => true]);
    $this->call('cache:clear');
    $this->call('optimize');
})->describe('ReSeed permissions, roles and their associations');

Artisan::command('db:seed:config', function () {
    $this->call('config:clear');
    $this->call('db:seed', ['--class' => 'DefaultConfigSeeder', '--force' => true]);
    $this->call('optimize');
})->describe('ReSeed configuration');

Artisan::command('exception:list', function () {
    $this->table(['Exception', 'Status', 'Message'], app('exceptions')->map(function ($class) {
        $e = (new \ReflectionClass($class))->newInstanceWithoutConstructor();
        return [
            class_basename($class),
            $e->status,
            $e->getLocalizedMessage(),
        ];
    })->toArray());
})->describe('List all custom exception with their translation and status code.');

Artisan::command('stripe:sync:subscription-plans', function (\App\Stripe\StripeManager $stripeManager) {
    Artisan::call('db:seed', ['--class' => \Database\Seeders\DefaultSubscriptionPlansSeeder::class]);
    if (app()->runningInConsole()) {
        $this->table(['Name', 'Description', 'Listings', 'Nicknames'], \App\Models\SubscriptionPlan::all()->map(function (\App\Models\SubscriptionPlan $plan) {
            return [$plan->name, $plan->description, $plan->listings_limit, implode(', ', array_column($plan->prices, 'nickname'))];
        }));
    }
});

Artisan::command('test', function () {
    $notifiable = User::latest()->first();
    echo URL::temporarySignedRoute(
        'verification.verify',
        Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
        [
            'id'   => $notifiable->getKey(),
            'hash' => sha1($notifiable->getEmailForVerification()),
        ]
    );
});
