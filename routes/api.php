<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum', 'throttle:erp', 'verified', 'broker'])->name('erp.')->prefix('erp')->group(function () {
    Route::put('properties/{property_id}/restore', [App\Http\Controllers\API\ERP\PropertyController::class, 'restore']);
    Route::post('properties/{property}/update', [App\Http\Controllers\API\ERP\PropertyController::class, 'update']);
    Route::apiResource('properties', App\Http\Controllers\API\ERP\PropertyController::class);
});

Route::middleware(['auth:sanctum', 'throttle:api'])->group(function () {
    Route::get('/user/current', [App\Http\Controllers\API\UserController::class, 'current'])->name('user.current');
    Route::get('/broker/current', [App\Http\Controllers\API\BrokerController::class, 'current'])->name('broker.current');
    Route::get('/property-attributes/by-category', [App\Http\Controllers\API\PropertyAttributesController::class, 'byCategory'])->name('property-attributes.by-category');
    Route::get('/property-attributes/category/{category}', [App\Http\Controllers\API\PropertyAttributesController::class, 'forCategory'])->name('property-attributes.for-category');
    Route::get('/districts', [App\Http\Controllers\API\DistrictController::class, 'index'])->name('districts.index');
    Route::get('/districts/{district}', [App\Http\Controllers\API\DistrictController::class, 'show'])->name('districts.show');
    Route::get('/municipalities', [App\Http\Controllers\API\MunicipalityController::class, 'index'])->name('municipalities.index');
    Route::get('/municipalities/{municipality}', [App\Http\Controllers\API\MunicipalityController::class, 'show'])->name('municipalities.show');
    Route::get('/vicinities', [App\Http\Controllers\API\VicinityController::class, 'index'])->name('vicinities.index');
    Route::get('/vicinities/{vicinity}', [App\Http\Controllers\API\VicinityController::class, 'show'])->name('vicinities.show');
});

Route::middleware(['localize'])->group(function () {
    Route::get('language/current', [\App\Http\Controllers\PublicFrontEndController::class, 'getCurrentLanguage']);
    Route::put('language/{code}', [\App\Http\Controllers\PublicFrontEndController::class, 'setLanguage']);
    Route::get('location/search', [\App\Http\Controllers\PublicFrontEndController::class, 'searchLocation']);
    Route::get('property/search', [\App\Http\Controllers\PublicFrontEndController::class, 'searchProperty']);
    Route::post('property/{property}/property-application', [\App\Http\Controllers\PublicFrontEndController::class, 'storePropertyApplication']);
    Route::get('property/{id}', [\App\Http\Controllers\PublicFrontEndController::class, 'showProperty']);
});
