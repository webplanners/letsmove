

## Stripe

https://stripe.com/docs/cli/events/resend

```
stripe events resend evt_1IDqP4DC1V6pYz1rW9cZ7dfg
stripe events resend evt_1IDqP4DC1V6pYz1rW9cZ7dfg --webhook-endpoint=<endpoint_id>
```

https://stripe.com/docs/stripe-cli/webhooks

```
stripe listen --forward-to letsmove.local:8080/stripe/webhook
```
