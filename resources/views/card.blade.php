<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Card') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if($intent)
                        <input id="card-holder-name" type="text">
                        <!-- Stripe Elements Placeholder -->
                        <div id="card-element"></div>
                        <button id="card-button" data-secret="{{ $intent->client_secret }}">
                            Update Payment Method
                        </button>
                        <script src="https://js.stripe.com/v3/"></script>
                        <script>
                            const stripe = Stripe('{{ config('cashier.key') }}');
                            var style = {
                                base: {
                                    iconColor: '#c4f0ff',
                                    color: '#fff',
                                    fontWeight: 500,
                                    fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                                    fontSize: '16px',
                                    fontSmoothing: 'antialiased',
                                    ':-webkit-autofill': {
                                        color: '#fce883',
                                    },
                                    '::placeholder': {
                                        color: '#87BBFD',
                                    },
                                },
                                invalid: {
                                    iconColor: '#FFC7EE',
                                    color: '#FFC7EE',
                                },
                            };
                            const elements = stripe.elements();
                            const cardElement = elements.create('card', {hidePostalCode: true, style: style});

                            cardElement.mount('#card-element');
                            const cardHolderName = document.getElementById('card-holder-name');
                            const cardButton = document.getElementById('card-button');
                            const clientSecret = cardButton.dataset.secret;

                            cardButton.addEventListener('click', async (e) => {
                                const {setupIntent, error} = await stripe.confirmCardSetup(
                                    clientSecret, {
                                        payment_method: {
                                            card           : cardElement,
                                            billing_details: {name: cardHolderName.value}
                                        }
                                    }
                                );

                                if (error) {
                                    console.error(error);
                                } else {
                                    console.log('success', setupIntent);
                                }
                            });
                        </script>

                    @else
                        Not a broker
                    @endif
                </div>
            </div>
        </div>
    </div>
    <style>
        .StripeElement {
            background-color: white;
            padding: 8px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }
        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }
        .StripeElement--invalid {
            border-color: #fa755a;
        }
        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
</x-app-layout>
