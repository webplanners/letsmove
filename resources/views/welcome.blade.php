<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #C41B3A;
            color: #fff;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
            background-image: url(http://gdpr.letsmove.gr/wp-content/uploads/2020/09/2560x1345_1.png);
            background-size: cover;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .bottom-right {
            position: absolute;
            right: 10px;
            bottom: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #ffffff29;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        #logo {
            max-width: 400px;
        }

        .c-content {
            background: #fff;
            padding: 30px 25px 40px 25px;
            border-radius: 15px;
            background: linear-gradient( 45deg, #b8b8b8, transparent);
        }

        .c-content .c-links a {
            color: #1B2032;
        }
    </style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="content c-content">
            <div class="title">
                <div class="logo-wrapper">
                    <img src="/img/letsmove-logo-black.png" alt="letsmove logo" id="logo">
                </div>
             </div>

            <div class="links c-links">
                <a href="{{ route('nova.broker.current') }}/">{{ __('Dashboard') }}</a>

                @auth
                <a href="{{ route('nova.logout') }}" class="block no-underline text-90 hover:bg-30 p-3">{{ __('Logout') }}</a>
                @else
                | <a href="{{ route('nova.login') }}">{{ __('Login') }}</a> |

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">{{ __('Register') }}</a>
                @endif
                @endauth
            </div>

            <div class="links bottom-right">
                <a href="https://documenter.getpostman.com/view/1408338/TW6wK9C9" target="_blank">Front-End API</a>
                <a href="https://documenter.getpostman.com/view/1408338/TVspm9qf" target="_blank">ERP API</a>
            </div>
        </div>
    </div>
</body>
</html>
