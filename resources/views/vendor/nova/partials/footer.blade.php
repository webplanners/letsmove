<p class="mt-8 text-center text-xs text-80">
    <a href="https://letsmove.gr" class="text-primary dim no-underline">LetsMove.gr</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} WebPlanners.gr
</p>
