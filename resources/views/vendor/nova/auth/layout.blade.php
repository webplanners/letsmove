{{-- Marshmallow Nova  --}}
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-full font-sans antialiased">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ \Laravel\Nova\Nova::name() }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('app.css', 'vendor/nova') }}">

    <!-- Custom Meta Data -->
    @include('nova::partials.meta')

    <!-- Theme Styles -->
    @foreach(\Laravel\Nova\Nova::themeStyles() as $publicPath)
        <link rel="stylesheet" href="{{ $publicPath }}">
    @endforeach

    <style>
        body {
            background-image: url(http://gdpr.letsmove.gr/wp-content/uploads/2020/09/2560x1345_1.png);
            background-size: cover;
        }

        form {
            border-radius: 15px;
            background: linear-gradient(45deg, #b8b8b8, transparent);
            background-color: transparent !important;
        }

        .btn-primary {
            background-color: #C41B39;
            color: #fff;
        }

        .btn-primary:hover {
            background-color: #c41b39bf !important;
            color: #fff;
        }

        .btn-default:not([disabled]):not(.btn-disabled):active, .btn-default:not([disabled]):not(.btn-disabled):focus {
            outline: 0;
            -webkit-box-shadow: 0 0 0 3px #C41B39;
            box-shadow: 0 0 0 3px #C41B39;
        }

        .text-primary {
            color: #1B2032;
        }
    </style>
</head>
<body class="bg-gray-100 text-black h-full">
    <div class="h-full">
        <div class="px-view py-view mx-auto">
            @yield('content')
        </div>
    </div>
</body>
</html>
