<?php
/**
 * PropertyAttributesController.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 3/12/20 6:58 μ.μ.
 */

namespace App\Support;


use App\Exceptions\InvalidDataException;
use App\Models\PropertyAttribute;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class PropertyAttributes
{
    public const TYPES = [
        'single_selection',
        'multiple_selection',
        'integer',
        'float',
        'boolean',
        'text',
        'date',
        'datetime',
    ];

    public const DB_TYPES = [
        'single_selection'   => 'string',
        'multiple_selection' => 'json',
        'integer'            => 'integer',
        'boolean'            => 'boolean',
        'float'              => 'float',
        'text'               => 'string',
        'date'               => 'date',
        'datetime'           => 'datetime',
    ];

    public const CASTS = [
//        'single_selection'   => 'string',
        'multiple_selection' => 'array',
//        'integer'            => 'integer',
        'boolean'            => 'boolean',
//        'float'              => 'float',
//        'text'               => 'string',
        'date'               => 'date',
        'datetime'           => 'datetime',
    ];

    /**
     * @return array
     */
    public static function parse()
    {
        return json_decode(file_get_contents(database_path('property-attributes.json')), true);
    }

    /**
     * @return string[]
     */
    public static function listTypes()
    {
        return array_combine(self::TYPES, array_map('humanize_attr', self::TYPES));
    }

    /**
     * @param string $type
     * @return string
     */
    public static function validateType($type)
    {
        throw_unless(in_array($type, self::TYPES), InvalidDataException::class, "Invalid property attribute type '$type'");
        return $type;
    }

    /**
     * @param array|Collection $options
     * @return array
     */
    public static function formatOptions($options)
    {
        return array_map('snake_case', array_map('studly_case', array_values($options)));
    }

    /**
     * @param PropertyAttribute $pa
     * @return \Laravel\Nova\Fields\Field
     */
    public static function buildNovaField(PropertyAttribute $pa)
    {
        switch ($pa->type) {
            case 'single_selection':
                $field = \Laravel\Nova\Fields\Select::make($pa->translated_name, $pa->code)->displayUsingLabels()->options($pa->translated_options)->sortable();
                break;
            case 'multiple_selection':
                $field = \Laravel\Nova\Fields\BooleanGroup::make($pa->translated_name, $pa->code)->options($pa->translated_options);
                break;
            case 'integer':
                $field = \Laravel\Nova\Fields\Number::make($pa->translated_name, $pa->code)->sortable();
                break;
            case 'float':
                $field = \Laravel\Nova\Fields\Number::make($pa->translated_name, $pa->code)->step(0.01)->sortable();
                break;
            case 'boolean':
                $field = \Laravel\Nova\Fields\Boolean::make($pa->translated_name, $pa->code)->sortable();
                break;
            case 'text':
                $field = \Laravel\Nova\Fields\Text::make($pa->translated_name, $pa->code);
                break;
            case 'date':
                $field = \Laravel\Nova\Fields\Date::make($pa->translated_name, $pa->code)->sortable()->resolveUsing(function ($value) {
                    return empty($value) ? null : new Carbon($value);
                })->nullable(!$pa->required);
//                ->fillUsing(function ($request, $model, $attribute, $requestAttribute) {
//                    if ($request->exists($requestAttribute)) {
//                        $value = $request[$requestAttribute];
//                        $model->{$attribute} = empty($value) ? null : new Carbon($value);
//                    }
//                });
                break;
            case 'datetime':
                $field = \Laravel\Nova\Fields\DateTime::make($pa->translated_name, $pa->code)->sortable()->resolveUsing(function ($value) {
                    return empty($value) ? null : new Carbon($value);
                })->nullable(!$pa->required);
//                    ->fillUsing(function ($request, $model, $attribute, $requestAttribute) {
//                        if ($request->exists($requestAttribute)) {
//                            $value = $request[$requestAttribute];
//                            $model->{$attribute} = empty($value) ? null : new Carbon($value);
//                        }
//                    });
                break;
            default:
                throw new \ErrorException("Unknown nova field type for property attribute type '{$pa->type}'");
        }
        $pa->addCasting();
        if ($pa->validation) {
            $field->rules($pa->validation_rules);
        }
        $field->required($pa->required);
        return $field;
    }

    public static function migrate()
    {
        Log::info('Migrating Property Attributes');
        return PropertyAttribute::with('property_category')->get()->filter(function (PropertyAttribute $pa) {
            return $pa->createDbColumn();
        })->count();
    }
}
