<?php
/**
 * MediaPathGenerator.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 19/12/20 12:39 π.μ.
 */

namespace App\Support;


use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\DefaultPathGenerator;

class MediaPathGenerator extends DefaultPathGenerator
{
    protected function getBasePath(Media $media): string
    {
        return $media->getCustomProperty('path', 'media') . DIRECTORY_SEPARATOR . $media->getKey();
    }
}
