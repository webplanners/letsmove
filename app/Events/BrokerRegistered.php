<?php

namespace App\Events;

use App\Models\Broker;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BrokerRegistered
{
    use Dispatchable, SerializesModels;

    public Broker $broker;

    public function __construct(Broker $broker)
    {
        $this->broker = $broker;
    }

}
