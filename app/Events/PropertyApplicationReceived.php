<?php

namespace App\Events;

use App\Models\PropertyApplication;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PropertyApplicationReceived
{
    use Dispatchable, SerializesModels;

    public PropertyApplication $pa;

    public function __construct(PropertyApplication $pa)
    {
        $this->pa = $pa;
    }

}
