<?php

namespace App\Providers;

use App\Models\Subscription;
use App\Observers\SubscriptionObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        \Illuminate\Auth\Events\Registered::class      => [
            \Illuminate\Auth\Listeners\SendEmailVerificationNotification::class,
        ],
        \Laravel\Cashier\Events\WebhookReceived::class => [
            \App\Listeners\LogWebhookReceived::class,
        ],

        \App\Events\BrokerRegistered::class            => [
            \App\Listeners\ SendBrokerWelcomeEmail::class,
            \App\Listeners\SendAdminBrokerRegisteredNotification::class,
        ],
        \App\Events\PropertyApplicationReceived::class => [
            \App\Listeners\NotifyBrokerForPropertyApplication::class,
        ],

//        'Illuminate\Auth\Events\Registered' => [
//            'App\Listeners\LogRegisteredUser',
//        ],
//        'Illuminate\Auth\Events\Attempting' => [
//            'App\Listeners\LogAuthenticationAttempt',
//        ],
//        'Illuminate\Auth\Events\Authenticated' => [
//            'App\Listeners\LogAuthenticated',
//        ],
//        'Illuminate\Auth\Events\Login' => [
//            'App\Listeners\LogSuccessfulLogin',
//        ],
//        'Illuminate\Auth\Events\Failed' => [
//            'App\Listeners\LogFailedLogin',
//        ],
//        'Illuminate\Auth\Events\Validated' => [
//            'App\Listeners\LogValidated',
//        ],
        \Illuminate\Auth\Events\Verified::class        => [
            \App\Listeners\SendMailVerified::class,
        ],
//        'Illuminate\Auth\Events\Logout' => [
//            'App\Listeners\LogSuccessfulLogout',
//        ],
//        'Illuminate\Auth\Events\CurrentDeviceLogout' => [
//            'App\Listeners\LogCurrentDeviceLogout',
//        ],
//        'Illuminate\Auth\Events\OtherDeviceLogout' => [
//            'App\Listeners\LogOtherDeviceLogout',
//        ],
//        'Illuminate\Auth\Events\Lockout' => [
//            'App\Listeners\LogLockout',
//        ],
//        'Illuminate\Auth\Events\PasswordReset' => [
//            'App\Listeners\LogPasswordReset',
//        ],
    ];

    public function boot()
    {
        Subscription::observe(SubscriptionObserver::class);
    }
}
