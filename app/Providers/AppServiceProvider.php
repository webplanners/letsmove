<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Resource;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(\App\Stripe\StripeManager::class, function ($app) {
            return new \App\Stripe\StripeManager();
        });
    }

    public function boot()
    {
        JsonResource::withoutWrapping();
        if (!App::configurationIsCached()) {
            try {
                config(Setting::getConfig());
            } catch (\RedisException $e) {
                if (str_slug($e->getMessage()) == 'connection-refused') {
                    Log::critical('REDIS DOWN!');
                }
            }
        }
        $this->app->singleton('installed', function () {
            return config('app.install_date');
        });
        Blueprint::macro('blameable', function () {
            $this->foreignId(config('blameable.column_names.createdByAttribute'))->nullable();
            $this->foreignId(config('blameable.column_names.updatedByAttribute'))->nullable();
            $this->foreignId(config('blameable.column_names.deletedByAttribute'))->nullable();
        });
        $this->configureValidationRules();
    }

    private function configureValidationRules()
    {
        Validator::extend('html', \App\Rules\ValidHtml::class . '@passes');
    }
}
