<?php

namespace App\Providers;

use App\Models\Actions\ActionEvent;
use App\Models\Broker;
use App\Models\District;
use App\Models\Language;
use App\Models\Municipality;
use App\Models\Permission;
use App\Models\Property;
use App\Models\PropertyApplication;
use App\Models\PropertyAttribute;
use App\Models\PropertyCategory;
use App\Models\Role;
use App\Models\Subscription;
use App\Models\SubscriptionItem;
use App\Models\SubscriptionPlan;
use App\Models\User;
use App\Models\Vicinity;
use App\Policies\ActionEventPolicy;
use App\Policies\BrokerPolicy;
use App\Policies\DistrictPolicy;
use App\Policies\LanguagePolicy;
use App\Policies\MunicipalityPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\PropertyApplicationPolicy;
use App\Policies\PropertyAttributePolicy;
use App\Policies\PropertyCategoryPolicy;
use App\Policies\PropertyPolicy;
use App\Policies\RolePolicy;
use App\Policies\SubscriptionItemPolicy;
use App\Policies\SubscriptionPlanPolicy;
use App\Policies\SubscriptionPolicy;
use App\Policies\UserPolicy;
use App\Policies\VicinityPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        User::class                => UserPolicy::class,
        Role::class                => RolePolicy::class,
        Permission::class          => PermissionPolicy::class,
        Language::class            => LanguagePolicy::class,
        Subscription::class        => SubscriptionPolicy::class,
        SubscriptionItem::class    => SubscriptionItemPolicy::class,
        SubscriptionPlan::class    => SubscriptionPlanPolicy::class,
        PropertyCategory::class    => PropertyCategoryPolicy::class,
        PropertyAttribute::class   => PropertyAttributePolicy::class,
        PropertyApplication::class => PropertyApplicationPolicy::class,
        Property::class            => PropertyPolicy::class,
        Broker::class              => BrokerPolicy::class,
        Vicinity::class            => VicinityPolicy::class,
        Municipality::class        => MunicipalityPolicy::class,
        District::class            => DistrictPolicy::class,
        ActionEvent::class         => ActionEventPolicy::class,
    ];

    public function boot()
    {
        $this->registerPolicies();
    }
}
