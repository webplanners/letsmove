<?php

namespace App\Providers;

use App\Nova\Dashboards\ServerAdmin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Fields\Code;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use Nalingia\SimpleToolbarLink\SimpleToolbarLink;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    public function boot()
    {
        parent::boot();
        $this->settingsTool();
    }

    private function settingsTool()
    {
        \OptimistDigital\NovaSettings\NovaSettings::addSettingsFields(function () {
            return [
                DateTime::make('Install Date', function () {
                    return config('app.install_date');
                })->readonly(),
                Text::make('Stripe/Publishable Key', 'cashier#key')
                    ->rules('nullable', 'starts_with:pk_')->placeholder(config('cashier.key'))
                    ->help('Με αυτό εκτελούνται οι κινήσεις στο front-end.'),
                Text::make('Stripe Secret', 'cashier#secret')
                    ->rules('nullable', 'starts_with:sk_')->placeholder(config('cashier.secret'))
                    ->help('Αυτό μένει κρυφό μόνο στον server για να επικοινωνεί με το Stripe.'),
                Text::make('Stripe Webhook Secret', 'cashier#webhook#secret')
                    ->rules('nullable', 'starts_with:whsec_')->placeholder(config('cashier.webhook.secret'))
                    ->help('Απαιτείται για να επαληθεύει τα μηνύματα από το Stripe.'),
                Code::make('HTML HEAD', 'nova#html#head')->language('htmlmixed')->rules('nullable', 'html')
                    ->help('After saving <b>do not reload</b>, instead <a href="/admin/" target="_blank">open in new tab</a> to validate.'),
                Code::make('HTML BODY', 'nova#html#body')->language('htmlmixed')->rules('nullable'),
            ];
        });
    }

    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return session('roles.admin') || session('roles.broker');
        });
    }

    protected function cards()
    {
        return [
            \Richardkeep\NovaTimenow\NovaTimenow::make()->timezones([config('app.timezone')]),
        ];
    }

    protected function dashboards()
    {
        return [
            ServerAdmin::make(),
        ];
    }

    public function tools()
    {
        if (Auth::user()->can('server-admin')) {
            return [
                \OptimistDigital\NovaSettings\NovaSettings::make(),
                \Joedixon\NovaTranslation\NovaTranslation::make()->canSeeWhen('translation-admin'), // PAID translate via admin panel
                \Infinety\Filemanager\FilemanagerTool::make(),
                \Davidpiesse\NovaPhpinfo\Tool::make(),
                \BinaryBuilds\NovaAdvancedCommandRunner\CommandRunner::make(),
                \Spatie\BackupTool\BackupTool::make(),
                \Insenseanalytics\NovaServerMonitor\NovaServerMonitor::make(),
                \Sbine\RouteViewer\RouteViewer::make(),
                \Spatie\TailTool\TailTool::make(),
                SimpleToolbarLink::make()->name('Telescope')->to('/telescope')->target('telescope')->icon('<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 80"><path fill="var(--sidebar-icon)" d="M0 40a39.87 39.87 0 0 1 11.72-28.28A40 40 0 1 1 0 40zm34 10a4 4 0 0 1-4-4v-2a2 2 0 1 0-4 0v2a4 4 0 0 1-4 4h-2a2 2 0 1 0 0 4h2a4 4 0 0 1 4 4v2a2 2 0 1 0 4 0v-2a4 4 0 0 1 4-4h2a2 2 0 1 0 0-4h-2zm24-24a6 6 0 0 1-6-6v-3a3 3 0 0 0-6 0v3a6 6 0 0 1-6 6h-3a3 3 0 0 0 0 6h3a6 6 0 0 1 6 6v3a3 3 0 0 0 6 0v-3a6 6 0 0 1 6-6h3a3 3 0 0 0 0-6h-3zm-4 36a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM21 28a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"></path></svg>'),
                SimpleToolbarLink::make()->name('Horizon')->to('/horizon')->target('horizon')->icon('<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path fill="var(--sidebar-icon)" d="M3.5,17.6C1.3,15.7,0,12.9,0,10c0-2.8,1.1-5.3,2.9-7.1C6.8-1,13.2-1,17.1,2.9s3.9,10.2,0,14.1 C13.4,20.8,7.5,21,3.5,17.6L3.5,17.6z M2.7,10.6c1.1-1,1.9-2.3,4-2.3c3.3,0,3.3,3.3,6.7,3.3c2.1,0,2.9-1.3,4-2.3 c-0.3-4-3.9-7-7.9-6.7S2.4,6.6,2.7,10.6L2.7,10.6z"/></svg>'),
            ];
        }
        return [
            \OptimistDigital\NovaSettings\NovaSettings::make()->canSeeWhen('admin'),
        ];
    }

    public function register()
    {
        //
    }
}
