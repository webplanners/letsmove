<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Telescope\EntryType;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    public function register()
    {
        $this->hideSensitiveRequestDetails();
        $this->tag();
        $this->filter();
    }

    protected function tag()
    {
        Telescope::tag(function (IncomingEntry $entry) {
            if ($entry->type === 'request') {
                $tags = collect(['status:' . $entry->content['response_status'], "method:{$entry->content['method']}"]);
                if (array_get($entry->content, 'duration') > 2000)
                    $tags->push('slow');
                if (in_array('nova', array_get($entry->content, 'middleware')))
                    $tags->push('nova');
                if ($route = \Route::current()) {
                    if ($route->named('stripe-webhook'))
                        $tags->push('stripe-webhook');
                    if ($route->named('api.*'))
                        $tags->push('api');
                    if ($route->named('*.erp.*'))
                        $tags->push('erp', 'erp:' . Auth::id(), 'broker:' . optional(Auth::user())->broker_id);
//                    if ($route->named('*.districts.*'))
//                        $tags->push('districts');
//                    if ($route->named('*.municipalities.*'))
//                        $tags->push('municipalities');
//                    if ($route->named('*.vicinities.*'))
//                        $tags->push('vicinities');
//                    if ($route->named('*.property-attributes.*'))
//                        $tags->push('property-attributes');
                    if ($property = $route->originalParameter('property'))
                        $tags->push("property:$property");
                }
                return $tags->toArray();
            }
            return [];
        });
    }

    protected function filter()
    {
        Telescope::filter(function (IncomingEntry $entry) {
            if ($this->app->environment('local')) {
                return true;
            }
            $filter = function () use ($entry) {
                switch ($entry->type) {
                    case EntryType::REQUEST:
                        if (array_get($entry->content, 'status') >= 400 || array_get($entry->content, 'duration') > 3000)
                            return true;
                        return !starts_with(array_get($entry->content, 'uri'), ['/nova-api/', '/nova-vendor/']);
                }
                return false;
            };
            return $entry->isReportableException() ||
                $entry->isFailedRequest() ||
                $entry->isFailedJob() ||
                $entry->isScheduledTask() ||
                $entry->hasMonitoredTag() ||
                $filter();
        });
    }

    protected function hideSensitiveRequestDetails()
    {
        if ($this->app->environment('local')) {
            return;
        }
        Telescope::hideRequestParameters(['_token']);
        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    protected function gate()
    {
        Gate::define('viewTelescope', function (User $user) {
            return $user->can('admin');
        });
    }
}
