<?php

namespace App\Models;

use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PropertyApplication
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $phone
 * @property float|null $negotiation_price
 * @property string|null $message
 * @property int $property_id
 * @property int $broker_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\Broker $broker
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User|null $deleter
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \App\Models\Property $property
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication query()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereBrokerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereNegotiationPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication wherePropertyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyApplication whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class PropertyApplication extends Model
{
    use ModelTraits;

    protected $fillable = ['name', 'email', 'phone', 'message', 'negotiation_price', 'property_id', 'broker_id'];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function broker()
    {
        return $this->belongsTo(Broker::class);
    }
}
