<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Carbon;
use Stripe\Subscription as StripeSubscription;

/**
 * App\Models\Subscription
 *
 * @property int                                                                          $id
 * @property int                                                                          $user_id
 * @property string                                                                       $name
 * @property string                                                                       $stripe_id
 * @property string                                                                       $stripe_status
 * @property string|null                                                                  $stripe_plan
 * @property string|null                                                                  $stripe_product
 * @property int|null                                                                     $quantity
 * @property Carbon|null                                                                  $trial_ends_at
 * @property Carbon|null                                                                  $ends_at
 * @property Carbon|null                                                                  $created_at
 * @property Carbon|null                                                                  $updated_at
 * @property-read int                                                                     $broker_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SubscriptionItem[] $items
 * @property-read int|null                                                                $items_count
 * @property-read \App\Models\SubscriptionPlan|null                                       $subscription_plan
 * @property-read \App\Models\Broker                                                      $user
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription active()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription cancelled()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription ended()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription incomplete()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription notCancelled()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription notOnGracePeriod()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription notOnTrial()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription onGracePeriod()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription onTrial()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription pastDue()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription recurring()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereStripePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereStripeProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereStripeStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription whereUserId($value)
 * @mixin \Eloquent
 */
class Subscription extends \Laravel\Cashier\Subscription
{
    /**
     * @param Broker $broker
     * @return static|\Illuminate\Database\Eloquent\Model
     */
    public static function forBroker(Broker $broker)
    {
        return static::newModelInstance()->broker()->associate($broker);
    }

    /**
     * @return int
     */
    public function getBrokerIdAttribute()
    {
        return $this->user_id;
    }

    /**
     * @return $this
     */
    public function syncFromStripe()
    {
        return $this->syncFromData($this->asStripeSubscription());
    }

    /**
     * Saves the underlying subscription and its subscription plans.
     * @param array|\Stripe\Subscription $data
     * @return $this
     */
    public function syncFromData($data)
    {
        // from \Laravel\Cashier\Http\Controllers\WebhookController::handleCustomerSubscriptionUpdated
        if (
            isset($data['status']) &&
            $data['status'] === StripeSubscription::STATUS_INCOMPLETE_EXPIRED
        ) {
            $this->items()->delete();
            $this->delete();
            return $this;
        }

        $firstItem = $data['items']['data'][0];
        $isSinglePlan = count($data['items']['data']) === 1;

        $ends_at = null;
        if (isset($data['cancel_at_period_end']) && $data['cancel_at_period_end']) {
            $ends_at = $this->onTrial()
                ? $this->trial_ends_at
                : Carbon::createFromTimestamp($data['current_period_end']);
        } elseif (!empty($data['cancel_at'])) {
            $ends_at = Carbon::createFromTimestamp($data['cancel_at']);
        }

        $this->forceFill([
            'name'           => $data['metadata']['name'] ?? 'default',
            'stripe_id'      => $data['id'],
            'stripe_status'  => $data['status'],
            'stripe_plan'    => array_get($data, 'plan.id') ?: ($isSinglePlan ? $firstItem['plan']['id'] : null),
            'stripe_product' => $data['plan']['product'],
            'quantity'       => $data['quantity'] ?? ($isSinglePlan && isset($firstItem['quantity']) ? $firstItem['quantity'] : null),
            'trial_ends_at'  => empty($data['trial_end']) ? null : Carbon::createFromTimestamp($data['trial_end']),
            'ends_at'        => $ends_at,
        ]);

        $this->save();

        $plans = [];
        foreach (array_get($data, 'items.data') as $item) {
            $plans[] = $item['plan']['id'];

            $this->items()->updateOrCreate([
                'stripe_id' => $item['id'],
            ], [
                'stripe_plan' => $item['plan']['id'],
                'quantity'    => $item['quantity'] ?? null,
            ]);
        }
        $this->items()->whereNotIn('stripe_plan', $plans)->delete();

        return $this;
    }

    public function broker()
    {
        return $this->belongsTo(Broker::class, 'user_id');
    }

    public function owner()
    {
        return parent::owner()->withoutGlobalScope(SoftDeletingScope::class);
    }

    public function items()
    {
        return $this->hasMany(\App\Models\SubscriptionItem::class);
    }

    public function subscription_plan()
    {
        return $this->belongsTo(SubscriptionPlan::class, 'stripe_product', 'stripe_product_id')->withoutGlobalScope(SoftDeletingScope::class);
    }
}
