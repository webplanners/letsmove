<?php

namespace App\Models;

/**
 * App\Models\SubscriptionItem
 *
 * @property int $id
 * @property int $subscription_id
 * @property string $stripe_id
 * @property string $stripe_plan
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Laravel\Cashier\Subscription $subscription
 * @property-read \App\Models\SubscriptionPlan $subscription_plan
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem whereStripePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem whereSubscriptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SubscriptionItem extends \Laravel\Cashier\SubscriptionItem
{

    public function subscription_plan()
    {
        return $this->belongsTo(SubscriptionPlan::class, 'stripe_plan', 'stripe_product_id');
    }
}
