<?php

namespace App\Models;

use App\Models\Traits\Area;
use App\Models\Traits\HasTranslatedName;
use App\Models\Traits\ModelCaching;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Municipality
 *
 * @property int                                                                             $id
 * @property string                                                                          $name_el
 * @property string                                                                          $name_en
 * @property int                                                                             $district_id
 * @property \Illuminate\Support\Carbon|null                                                 $created_at
 * @property \Illuminate\Support\Carbon|null                                                 $updated_at
 * @property \Illuminate\Support\Carbon|null                                                 $deleted_at
 * @property int|null                                                                        $created_by
 * @property int|null                                                                        $updated_by
 * @property int|null                                                                        $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null                                                                   $actions_count
 * @property-read \App\Models\User|null                                                      $creator
 * @property-read \App\Models\User|null                                                      $deleter
 * @property-read \App\Models\District                                                       $district
 * @property-read string                                                                     $name
 * @property-read string                                                                     $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[]   $notes
 * @property-read int|null                                                                   $notes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Property[]            $properties
 * @property-read int|null                                                                   $properties_count
 * @property-read \App\Models\User|null                                                      $updater
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vicinity[]            $vicinities
 * @property-read int|null                                                                   $vicinities_count
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality query()
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereNameEl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Municipality whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class Municipality extends Model
{
    use ModelTraits;
    use HasTranslatedName;
    use ModelCaching;
    use Area;

    protected $fillable = ['id', 'name_el', 'name_en', 'district_id'];

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function vicinities()
    {
        return $this->hasMany(Vicinity::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
