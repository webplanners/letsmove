<?php
/**
 * Area.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 30/1/21 2:36 π.μ.
 */

namespace App\Models\Traits;


use App\Models\District;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Model
 */
trait Area
{

    /**
     * @param array|mixed|string[] $columns
     * @return District|District[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function all($columns = ['*'])
    {
        return static::cache(__FUNCTION__, function () {
            return parent::all();
        });
    }

    public static function searchName($name)
    {
        return static::where('name_en', 'like', "%$name%")
            ->orWhere('name_el', 'like', "%$name%");
    }

    /**
     * @return string
     */
    public function getTypeAttribute()
    {
        return strtolower(class_basename(static::class));
    }
}
