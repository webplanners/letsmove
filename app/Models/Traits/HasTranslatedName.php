<?php
/**
 * HasTranslatedName.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 5/12/20 11:50 μ.μ.
 */

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Model
 */
trait HasTranslatedName
{
    /**
     * @param null|string $language
     * @return string
     */
    public function getName($language, $backup_language = 'en')
    {
        return array_key_exists("name_$language", $this->attributes)
            ? $this->{"name_$language"}
            : $this->{"name_$backup_language"};
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->getName(app()->getLocale());
    }

}
