<?php
/**
 * Billable.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 4/12/20 12:05 π.μ.
 */

namespace App\Models\Traits;

/**
 * @method  \App\Models\Subscription subscription($name = 'default')
 * @mixin \Illuminate\Database\Eloquent\Model
 */
trait Billable
{
    use \Laravel\Cashier\Billable;

    public function subscriptions()
    {
        return $this->hasMany(\App\Models\Subscription::class, 'user_id')->orderBy('created_at', 'desc');
    }

}
