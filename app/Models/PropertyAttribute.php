<?php

namespace App\Models;

use App\Exceptions\InvalidDataException;
use App\Models\Traits\ModelCaching;
use App\Models\Traits\ModelTraits;
use App\Support\PropertyAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

/**
 * App\Models\PropertyAttribute
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $type
 * @property array $options
 * @property string|null $group
 * @property string $validation
 * @property int $property_category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User|null $deleter
 * @property-read string $db_type
 * @property-read \Laravel\Nova\Fields\Field $nova_field
 * @property-read bool $required
 * @property-read string $translated_name
 * @property-read string[] $translated_options
 * @property-read string[] $validation_rules
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \App\Models\PropertyCategory $property_category
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute query()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute wherePropertyCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyAttribute whereValidation($value)
 * @mixin \Eloquent
 */
class PropertyAttribute extends Model
{
    use ModelTraits;
    use ModelCaching;

    protected $attributes = [
        'options' => '[]',
    ];
    protected $casts = [
        'options' => 'array',
    ];

    /**
     * @param array|mixed|string[] $columns
     * @return PropertyAttribute|PropertyAttribute[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function all($columns = ['*'])
    {
        return static::cache('all', function () {
            return parent::all();
        });
    }

    /**
     * @param PropertyCategory|integer $category
     * @return PropertyAttribute[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function forCategory($category)
    {
        $id = is_object($category) ? $category->id : $category;
        return static::cache("PAs-category-$id", function () use ($id) {
            return static::wherePropertyCategoryId($id)->get();
        });
    }

    /**
     * @return string[]|Collection
     */
    public static function getGroups()
    {
        return static::cache('PA-groups', function () {
            return static::distinct()->pluck('group');
        });
    }

    /**
     * @param $code
     * @return PropertyAttribute
     */
    public static function findByCode($code)
    {
        if ($model = static::all()->firstWhere('code', $code)) {
            return $model;
        }
        throw new ModelNotFoundException("No PropertyAttribute found for code '$code'");
    }

    public function addCasting()
    {
        if ($cast = array_get(PropertyAttributes::CASTS, $this->type)) {
            if (in_array($cast, ['date', 'datetime'])) {
                Property::$_dates[] = $this->code;
            } else {
                Property::$_casts[$this->code] = $cast;
            }
        }
    }

    /**
     * @param string $type
     * @return string
     */
    public function setTypeAttribute($type)
    {
        return $this->attributes['type'] = PropertyAttributes::validateType($type);
    }

    /**
     * @param Property $p
     * @param array    $data
     * @return Property
     */
    public function fillProperty(Property $p, $data)
    {
        return tap($p)->setAttribute($this->code, array_get($data, $this->code));
    }

    /**
     * @param $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate($data)
    {
        return Validator::validate($data, [$this->code => $this->validation]);
    }

    /**
     * @return string[]
     */
    public function getValidationRulesAttribute()
    {
        return explode('|', $this->validation);
    }

    /**
     * @param string $option
     * @return string
     */
    public function validateOption($option)
    {
        throw_unless(in_array($option, array_column($this->options, 'code')), InvalidDataException::class, "Invalid option '$option' for {$this->name}");
        return $option;
    }

    /**
     * @return bool True if created, False if not because it already exists.
     * @throws \Throwable When db column exists but does not meet the requirements.
     */
    public function createDbColumn()
    {
        $db_type = $this->db_type;
        if (Schema::hasColumn('properties', $this->code)) {
            throw_unless(($type = Schema::getColumnType('properties', $this->code)) == $db_type, \ErrorException::class, "Property column '{$this->property_category->code}.{$this->code}' exists with type '$type' instead of '$db_type'");
            return false;
        }
        Schema::table('properties', function (Blueprint $table) use ($db_type) {
            // always nullable because PA from one PC are absent in other PC
            $table->{$db_type}($this->code)->nullable();
        });
        Log::info("Created PA column {$this->property_category->code}.{$this->code}($db_type)");
        return true;
    }

    /**
     * @return string
     */
    public function getDbTypeAttribute()
    {
        $type = PropertyAttributes::DB_TYPES[$this->type];
        switch ($type) {
            case 'string':
                $large = array_first($this->validation_rules, function ($v) {
                    return starts_with($v, 'max:');
                });
                if ($large) {
                    $large = explode(':', $large);
                    $large = $large[1];
                    if (is_numeric($large) && $large > 255)
                        return 'text';
                }
                break;
        }
        return $type;
    }

    /**
     * @return bool
     */
    public function getRequiredAttribute()
    {
        return in_array('required', $this->validation_rules);
    }

    /**
     * @return string
     */
    public function getTranslatedNameAttribute()
    {
        return __($this->name);
    }

    /**
     * @param array|Collection $options
     * @return string[]
     */
    public function setOptionsAttribute($options)
    {
        $this->attributes['options'] = json_encode(PropertyAttributes::formatOptions($options));
        return $this->options;
    }

    /**
     * @return string[]
     */
    public function getTranslatedOptionsAttribute()
    {
        return array_combine($this->options, array_map('__', $this->options));
//        return array_map('__', $this->options);
    }

    /**
     * @return \Laravel\Nova\Fields\Field
     */
    public function getNovaFieldAttribute()
    {
        return PropertyAttributes::buildNovaField($this);
    }

    public function property_category()
    {
        return $this->belongsTo(PropertyCategory::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

}
