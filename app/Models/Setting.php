<?php

namespace App\Models;

use App\Models\Traits\ModelCaching;
use OptimistDigital\NovaSettings\Models\Settings;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * App\Models\Setting
 *
 * @property string $key
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \App\Models\User|null $creator
 * @property-read string $config_key
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Setting createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereValue($value)
 * @mixin \Eloquent
 */
class Setting extends Settings
{
    protected $table = 'settings';
    public $timestamps = true;
    public $fillable = ['key', 'value'];

    use BlameableTrait;
    use ModelCaching;

    /**
     * @param array|mixed|string[] $columns
     * @return Setting|Setting[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function all($columns = ['*'])
    {
        return static::cache('all', function () {
            return parent::all();
        });
    }

    /**
     * @return array
     */
    public static function getConfig()
    {
        try {
            return static::cache('Setting-getConfig', function () {
                return static::all()->mapWithKeys(function (Setting $setting) {
                    return [$setting->config_key => $setting->value];
                })->all();
            });
        } catch (\PDOException $e) {
            // on initial run the table does not exist
            throw_unless($e->getCode() == '42S02', $e);
            return [];
        }
    }

    /**
     * @return string
     */
    public function getConfigKeyAttribute()
    {
        return str_replace('#', '.', $this->key);
    }

}
