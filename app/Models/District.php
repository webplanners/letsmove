<?php

namespace App\Models;

use App\Models\Traits\HasTranslatedName;
use App\Models\Traits\Area;
use App\Models\Traits\ModelCaching;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\District
 *
 * @property int $id
 * @property string $name_el
 * @property string $name_en
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User|null $deleter
 * @property-read string $name
 * @property-read string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Municipality[] $municipalities
 * @property-read int|null $municipalities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Property[] $properties
 * @property-read int|null $properties_count
 * @property-read \App\Models\User|null $updater
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vicinity[] $vicinities
 * @property-read int|null $vicinities_count
 * @method static \Illuminate\Database\Eloquent\Builder|District createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|District newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|District newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|District query()
 * @method static \Illuminate\Database\Eloquent\Builder|District updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereNameEl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|District whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class District extends Model
{
    use ModelTraits;
    use HasTranslatedName;
    use ModelCaching;
    use Area;

    protected $fillable = ['id', 'name_el', 'name_en'];

    public function municipalities()
    {
        return $this->hasMany(Municipality::class);
    }

    public function vicinities()
    {
        return $this->hasManyThrough(Vicinity::class, Municipality::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
