<?php

namespace App\Models;

use App\Models\Traits\ModelCaching;
use App\Models\Traits\ModelTraits;
use App\Stripe\StripeManager;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SubscriptionPlan
 *
 * @property int                                                                             $id
 * @property string                                                                          $name
 * @property string                                                                          $description
 * @property int                                                                             $listings_limit
 * @property string                                                                          $stripe_product_id
 * @property array                                                                           $prices
 * @property \Illuminate\Support\Carbon|null                                                 $created_at
 * @property \Illuminate\Support\Carbon|null                                                 $updated_at
 * @property \Illuminate\Support\Carbon|null                                                 $deleted_at
 * @property int|null                                                                        $created_by
 * @property int|null                                                                        $updated_by
 * @property int|null                                                                        $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null                                                                   $actions_count
 * @property-read \App\Models\User|null                                                      $creator
 * @property-read \App\Models\User|null                                                      $deleter
 * @property-read string                                                                     $display_name
 * @property-read string                                                                     $monthly_display_name
 * @property-read float                                                                      $monthly_language_price
 * @property-read float                                                                      $monthly_price
 * @property-read array                                                                      $monthly_stripe_price
 * @property-read string                                                                     $yearly_display_name
 * @property-read float                                                                      $yearly_language_price
 * @property-read float                                                                      $yearly_price
 * @property-read array                                                                      $yearly_stripe_price
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[]   $notes
 * @property-read int|null                                                                   $notes_count
 * @property-read \App\Models\User|null                                                      $updater
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereListingsLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan wherePrices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereStripeProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class SubscriptionPlan extends Model
{
    use ModelTraits;
    use ModelCaching;

    protected $fillable = ['stripe_product_id'];

    protected $attributes = [
        'prices' => '{}',
    ];

    protected $casts = [
        'prices' => 'array',
    ];

    /**
     * @param array|mixed|string[] $columns
     * @return SubscriptionPlan|SubscriptionPlan[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function all($columns = ['*'])
    {
        return static::cache('all', function () {
            return parent::all();
        });
    }

    public static function findByStripeId($product_id)
    {
        return static::whereStripeProductId($product_id)->firstOrFail();
    }

    /**
     * @param int $limit
     * @return SubscriptionPlan
     */
    public static function findByListingLimit($limit)
    {
        return static::whereListingsLimit($limit)->latest()->firstOrFail();
    }

    /**
     * @return string
     */
    public function getDisplayNameAttribute()
    {
        return "{$this->name} - " . __('Listings') . " {$this->listings_limit}";
    }

    /**
     * @return $this
     */
    public function syncFromStripe()
    {
        try {
            return $this->syncFromData(app(StripeManager::class)->client()->products->retrieve($this->stripe_product_id));
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            if (starts_with($e->getMessage(), 'No such product')) {
                $this->delete();
                return $this;
            }
            throw $e;
        }
    }

    /**
     * Saves (creates/updates) this subscription plan and deletes it if it is not active.
     * @param array $data
     * @return $this
     */
    public function syncFromData($data)
    {
        throw_if($data['metadata']['product'] != StripeManager::SUBSCRIPTION_PLAN_PRODUCT, \Exception::class, 'Product is not a subscription plan. Discarding update.');
        throw_if(empty($data['metadata']['listings']), \Exception::class, "Subscription Plan {$this->stripe_product_id} has empty 'metadata.listings'.");
        $this->forceFill([
            'stripe_product_id' => $data['id'],
            'name'              => $data['name'],
            'description'       => $data['description'],
            'listings_limit'    => $data['metadata']['listings'],
        ]);
        $this->updatePrices();
        $this->saveOrFail();
        if (!$data['active']) {
            $this->delete();
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function updatePrices()
    {
        $this->prices = app(StripeManager::class)->retrievePricesForProduct($this->stripe_product_id);
        return $this;
    }

    /**
     * @param string|null $key
     * @return array
     */
    public function getMonthlyStripePriceAttribute($key = null)
    {
        return array_get(collect($this->prices)->where('recurring.interval', 'month')->first(), $key);
    }

    /**
     * @return float
     */
    public function getMonthlyPriceAttribute()
    {
        return collect($this->getMonthlyStripePriceAttribute('tiers'))->where('up_to', 1)->first()['unit_amount'] / 100;
    }

    /**
     * @return float
     */
    public function getMonthlyLanguagePriceAttribute()
    {
        return collect($this->getMonthlyStripePriceAttribute('tiers'))->whereNull('up_to')->first()['unit_amount'] / 100;
    }

    /**
     * @return string
     */
    public function getMonthlyDisplayNameAttribute()
    {
        return __('Monthly') . " {$this->monthly_price}€ + {$this->monthly_language_price}€/" . __('Extra Language');
    }

    /**
     * @param string|null $key
     * @return array
     */
    public function getYearlyStripePriceAttribute($key = null)
    {
        return array_get(collect($this->prices)->where('recurring.interval', 'year')->first(), $key);
    }

    /**
     * @return float
     */
    public function getYearlyPriceAttribute()
    {
        return collect($this->getYearlyStripePriceAttribute('tiers'))->where('up_to', 1)->first()['unit_amount'] / 100;
    }

    /**
     * @return float
     */
    public function getYearlyLanguagePriceAttribute()
    {
        return collect($this->getYearlyStripePriceAttribute('tiers'))->whereNull('up_to')->first()['unit_amount'] / 100;
    }

    /**
     * @return string
     */
    public function getYearlyDisplayNameAttribute()
    {
        return __('Yearly') . " {$this->yearly_price}€ + {$this->yearly_language_price}€/" . __('Extra Language');
    }

}
