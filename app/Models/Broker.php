<?php

namespace App\Models;

use App\Exceptions\InvalidDataException;
use App\Models\Traits\Billable;
use App\Models\Traits\ModelTraits;
use App\Stripe\StripeManager;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Exceptions\CustomerAlreadyCreated;
use Laravel\Cashier\Exceptions\InvalidCustomer;
use Stripe\Customer as StripeCustomer;

/**
 * App\Models\Broker
 *
 * @property int                                                                                                            $id
 * @property string                                                                                                         $name
 * @property string                                                                                                         $status
 * @property string|null                                                                                                    $vin
 * @property string|null                                                                                                    $commercial_registration_number
 * @property string|null                                                                                                    $description
 * @property string                                                                                                         $type
 * @property string|null                                                                                                    $logo
 * @property string                                                                                                         $address
 * @property string                                                                                                         $phone
 * @property string|null                                                                                                    $phone_2
 * @property string|null                                                                                                    $email
 * @property string|null                                                                                                    $website
 * @property string|null                                                                                                    $facebook_username
 * @property string|null                                                                                                    $instagram_username
 * @property \Illuminate\Support\Carbon|null                                                                                $created_at
 * @property \Illuminate\Support\Carbon|null                                                                                $updated_at
 * @property \Illuminate\Support\Carbon|null                                                                                $deleted_at
 * @property int|null                                                                                                       $created_by
 * @property int|null                                                                                                       $updated_by
 * @property int|null                                                                                                       $deleted_by
 * @property string|null                                                                                                    $stripe_id
 * @property string|null                                                                                                    $card_brand
 * @property string|null                                                                                                    $card_last_four
 * @property string|null                                                                                                    $trial_ends_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[]                                $actions
 * @property-read int|null                                                                                                  $actions_count
 * @property-read \App\Models\User|null                                                                                     $creator
 * @property-read \App\Models\User|null                                                                                     $deleter
 * @property-read int                                                                                                       $broker_id
 * @property-read string|null                                                                                               $broker_stripe_id
 * @property-read string                                                                                                    $facebook_url
 * @property-read string                                                                                                    $instagram_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Language[]                                           $languages
 * @property-read int|null                                                                                                  $languages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[]                                  $notes
 * @property-read int|null                                                                                                  $notes_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null                                                                                                  $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Property[]                                           $properties
 * @property-read int|null                                                                                                  $properties_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyApplication[]                                $property_applications
 * @property-read int|null                                                                                                  $property_applications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subscription[]                                       $subscriptions
 * @property-read int|null                                                                                                  $subscriptions_count
 * @property-read \App\Models\User|null                                                                                     $updater
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[]                                               $users
 * @property-read int|null                                                                                                  $users_count
 * @method static Builder|Broker active()
 * @method static Builder|Broker createdBy($userId)
 * @method static Builder|Broker newModelQuery()
 * @method static Builder|Broker newQuery()
 * @method static Builder|Broker query()
 * @method static Builder|Broker updatedBy($userId)
 * @method static Builder|Broker whereAddress($value)
 * @method static Builder|Broker whereCardBrand($value)
 * @method static Builder|Broker whereCardLastFour($value)
 * @method static Builder|Broker whereCommercialRegistrationNumber($value)
 * @method static Builder|Broker whereCreatedAt($value)
 * @method static Builder|Broker whereCreatedBy($value)
 * @method static Builder|Broker whereDeletedAt($value)
 * @method static Builder|Broker whereDeletedBy($value)
 * @method static Builder|Broker whereDescription($value)
 * @method static Builder|Broker whereEmail($value)
 * @method static Builder|Broker whereFacebookUsername($value)
 * @method static Builder|Broker whereId($value)
 * @method static Builder|Broker whereInstagramUsername($value)
 * @method static Builder|Broker whereLogo($value)
 * @method static Builder|Broker whereName($value)
 * @method static Builder|Broker wherePhone($value)
 * @method static Builder|Broker wherePhone2($value)
 * @method static Builder|Broker whereStatus($value)
 * @method static Builder|Broker whereStripeId($value)
 * @method static Builder|Broker whereTrialEndsAt($value)
 * @method static Builder|Broker whereType($value)
 * @method static Builder|Broker whereUpdatedAt($value)
 * @method static Builder|Broker whereUpdatedBy($value)
 * @method static Builder|Broker whereVin($value)
 * @method static Builder|Broker whereWebsite($value)
 * @method static Builder|Broker withLanguage($language)
 * @mixin \Eloquent
 */
class Broker extends Model
{
    public const STATUSES = ['unverified', 'active', 'inactive'];
    public const TYPES = ['broker', 'constructor', 'civilian'];
    use ModelTraits;
    use Billable;
    use Notifiable;

    protected $fillable = ['name', 'type', 'address', 'phone', 'commercial_registration_number', 'vin'];
    protected $attributes = [
        'status' => 'unverified',
    ];

    /**
     * @return string[]
     */
    public static function listTypes()
    {
        return array_combine(self::TYPES, array_map('humanize_attr', self::TYPES));
    }

    /**
     * @return string[]
     */
    public static function listStatuses()
    {
        return array_combine(self::STATUSES, array_map('humanize_attr', self::STATUSES));
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('status', 'active')
            ->whereNull('deleted_at');
    }

    /**
     * @param Builder             $query
     * @param Language|string|int $language
     * @return Builder
     */
    public function scopeWithLanguage(Builder $query, $language)
    {
        return $query->when(!$language->default, function (Builder $query) use ($language) {
            return $query->whereHas('languages', function (Builder $q) use ($language) {
                return (is_numeric($language) | is_object($language)) ?
                    $q->where('id', is_object($language) ? $language->id : $language) :
                    $q->where('code', $language);
            });
        });
    }

    /**
     * @return int
     */
    public function getBrokerIdAttribute()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getBrokerStripeIdAttribute()
    {
        return $this->stripe_id;
    }


    /**
     * Saves the model
     * @param bool $verify_id
     * @return $this
     */
    public function syncStripeIdFromStripe($verify_id = true)
    {
        if ($this->hasStripeId()) {
            throw CustomerAlreadyCreated::exists($this);
        }
        $customers = app(StripeManager::class)->client()->customers->all(['limit' => 100, 'email' => $this->email]);
        foreach ($customers as $customer) {
            $customer = $customer['data'];
            if (!$verify_id) {
                $this->stripe_id = $customer['id'];
                break;
            }
            if (array_get($customer, 'metadata.broker_id') == $this->id) {
                $this->stripe_id = $customer['id'];
            }
        }
        $this->save();
        return $this;
    }

    /**
     * @return $this
     */
    public function syncSubscriptionsFromStripe()
    {
        if (!$this->hasStripeId()) {
            throw InvalidCustomer::notYetCreated($this);
        }
        $stripe_subscriptions = app(StripeManager::class)->client()->subscriptions->all(['limit' => 100, 'customer' => $this->stripe_id]);
        foreach ($stripe_subscriptions as $stripe_subscription) {
            /** @var Subscription $subscription */
            $subscription = $this->subscriptions()->firstOrNew(['stripe_id' => $stripe_subscription['id']]);
            $subscription->syncFromData($stripe_subscription);
        }
        return $this;
    }

    public function createAsStripeCustomer(array $options = [])
    {
        // \Laravel\Cashier\Concerns\ManagesCustomer::createAsStripeCustomer
        if ($this->hasStripeId()) {
            throw CustomerAlreadyCreated::exists($this);
        }
        // https://stripe.com/docs/api/customers/update
        $options = array_merge([
            'email'    => $this->email ?: $this->users()->whereNotNull('email')->first()->email,
            'name'     => $this->name,
            'phone'    => $this->phone,
            'metadata' => [
                'broker_id' => $this->id,
                'vin'       => $this->vin,
            ],
        ], $options);
        $customer = StripeCustomer::create($options, $this->stripeOptions());
        $this->stripe_id = $customer->id;
        $this->save();
        return $customer;
    }

    /**
     * @param string $status
     * @return string
     */
    public function setStatusAttribute($status)
    {
        throw_unless(in_array($status, static::STATUSES), InvalidDataException::class, "Invalid broker status '$status'");
        return $this->attributes['status'] = $status;
    }

    /**
     * @param string $type
     * @return string
     */
    public function setTypeAttribute($type)
    {
        throw_unless(in_array($type, static::TYPES), InvalidDataException::class, "Invalid broker type '$type'");
        return $this->attributes['type'] = $type;
    }

    /**
     * @return int
     */
    public function getRemainingLanguages()
    {
        if (empty($s = $this->subscription()) || !$s->valid())
            return 0;
        return $s->quantity - 1 - $this->languages->count();
    }

    /**
     * @return int
     */
    public function getRemainingProperties()
    {
        if (empty($s = $this->subscription()) || !$s->valid())
            return 0;
        return $s->subscription_plan->listings_limit - $this->properties()->count();
    }

    /**
     * @return string
     */
    public function getFacebookUrlAttribute()
    {
        return "https://www.facebook.com/" . e($this->facebook_username);
    }

    /**
     * @return string
     */
    public function getInstagramUrlAttribute()
    {
        return "https://www.instagram.com/" . e($this->instagram_username);
    }

    public function routeNotificationForMail($notification = null)
    {
        return $this->users()->pluck('email')->filter();
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function property_applications()
    {
        return $this->hasMany(PropertyApplication::class);
    }
}
