<?php

namespace App\Models;

use App\Models\Traits\ModelCaching;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Panel;

/**
 * App\Models\PropertyCategory
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User|null $deleter
 * @property-read string $translated_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Property[] $properties
 * @property-read int|null $properties_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyAttribute[] $property_attributes
 * @property-read int|null $property_attributes_count
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyCategory whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class PropertyCategory extends Model
{
    use ModelTraits;
    use ModelCaching;

    /**
     * @param array|mixed|string[] $columns
     * @return PropertyCategory|PropertyCategory[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public static function all($columns = ['*'])
    {
        return static::cache('all', function () {
            return parent::all();
        });
    }

    /**
     * @param Property $p
     * @param array    $data
     * @return Property
     */
    public function fillProperty(Property $p, $data)
    {
        $p->property_category()->associate($this);
        $this->getPropertyAttributes()->each(function (PropertyAttribute $pa) use ($p, $data) {
            $pa->fillProperty($p, $data);
        });
        return $p;
    }

    /**
     * @return string[]|\Illuminate\Support\Collection
     */
    public function getValidations()
    {
        return $this->getPropertyAttributes()->mapWithKeys(function (PropertyAttribute $pa) {
            return [$pa->code => $pa->validation];
        });
    }

    /**
     * @return string[]|\Illuminate\Support\Collection
     */
    public function listAttributeNames()
    {
        return $this->getPropertyAttributes()->mapWithKeys(function (PropertyAttribute $pa) {
            return [$pa->code => $pa->translated_name];
        });
    }

    /**
     * @return Panel[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function getNovaFields()
    {
        return $this->getPropertyAttributes()->groupBy('group')->map(function ($pas, $group) {
            return new Panel(humanize_attr($group), function () use ($pas) {
                return $pas->map->nova_field;
            });
        });
    }

    /**
     * @return PropertyAttribute[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getPropertyAttributes()
    {
        return PropertyAttribute::forCategory($this);
    }

    /**
     * @return string
     */
    public function getTranslatedNameAttribute()
    {
        return __($this->name);
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function property_attributes()
    {
        return $this->hasMany(PropertyAttribute::class);
    }

}
