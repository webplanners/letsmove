<?php

namespace App\Models;

use App\Models\Traits\ModelCaching;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Language
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Broker[] $brokers
 * @property-read int|null $brokers_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User|null $deleter
 * @property-read string $localized_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Language createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Language query()
 * @method static \Illuminate\Database\Eloquent\Builder|Language updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class Language extends Model
{
    use ModelTraits;
    use ModelCaching;

    /**
     * @return Language
     */
    public static function default()
    {
        return static::cache(__FUNCTION__, function () {
            return static::where('code', 'el')->first();
        });
    }

    /**
     * @return \Illuminate\Support\Collection|static[]
     */
    public static function defaults()
    {
        return static::cache(__FUNCTION__, function () {
            return static::where('default', true)->get();
        });
    }

    /**
     * Default languages are placed first
     * @return string[]
     */
    public static function codes()
    {
        return static::cache(__FUNCTION__, function () {
            return static::all()->sortByDesc('default')->pluck('code')->all();
        });
    }

    /**
     * @param string $code
     * @return Language
     */
    public static function findByCode($code)
    {
        return static::where('code', $code)->firstOrFail();
    }

    /**
     * @return string
     */
    public function getLocalizedNameAttribute()
    {
        return __($this->name);
    }

    public function brokers()
    {
        return $this->belongsToMany(Broker::class);
    }

}
