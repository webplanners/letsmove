<?php

namespace App\Models;

use App\Models\Traits\HasTranslatedName;
use App\Models\Traits\Area;
use App\Models\Traits\ModelCaching;
use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;

/**
 * App\Models\Vicinity
 *
 * @property int $id
 * @property string $name_el
 * @property string $name_en
 * @property int $municipality_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[] $actions
 * @property-read int|null $actions_count
 * @property-read \App\Models\User|null $creator
 * @property-read \App\Models\User|null $deleter
 * @property-read string $name
 * @property-read string $type
 * @property-read \App\Models\Municipality $municipality
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[] $notes
 * @property-read int|null $notes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Property[] $properties
 * @property-read int|null $properties_count
 * @property-read \App\Models\User|null $updater
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity createdBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity updatedBy($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereMunicipalityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereNameEl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vicinity whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class Vicinity extends Model
{
    use ModelTraits;
    use HasTranslatedName;
    use ModelCaching;
    use Area;

    protected $fillable = ['id', 'name_el', 'name_en', 'municipality_id'];

    public function municipality()
    {
        return $this->belongsTo(Municipality::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

    public function district()
    {
        return $this->belongsToThrough(District::class, Municipality::class)->withTrashed('districts.deleted_at', 'municipalities.deleted_at');
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

}
