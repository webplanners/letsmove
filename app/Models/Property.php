<?php

namespace App\Models;

use App\Models\Traits\ModelTraits;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * App\Models\Property
 *
 * @property int                                                                                   $id
 * @property int                                                                                   $broker_id
 * @property int                                                                                   $property_category_id
 * @property int                                                                                   $district_id
 * @property int                                                                                   $municipality_id
 * @property int                                                                                   $vicinity_id
 * @property string|null                                                                           $youtube
 * @property string|null                                                                           $description
 * @property \Illuminate\Support\Carbon|null                                                       $created_at
 * @property \Illuminate\Support\Carbon|null                                                       $updated_at
 * @property \Illuminate\Support\Carbon|null                                                       $deleted_at
 * @property int|null                                                                              $created_by
 * @property int|null                                                                              $updated_by
 * @property int|null                                                                              $deleted_by
 * @property string|null                                                                           $supply_type
 * @property string|null                                                                           $subcategory
 * @property string|null                                                                           $subregion
 * @property string|null                                                                           $real_address
 * @property string|null                                                                           $postcode
 * @property string|null                                                                           $displayed_address
 * @property int|null                                                                              $price
 * @property int|null                                                                              $sqm
 * @property string|null                                                                           $available_from
 * @property string|null                                                                           $construction_year
 * @property string|null                                                                           $floor
 * @property int|null                                                                              $level
 * @property int|null                                                                              $bedrooms
 * @property int|null                                                                              $living_room
 * @property int|null                                                                              $kitchen_room
 * @property int|null                                                                              $bathroom
 * @property int|null                                                                              $wc
 * @property string|null                                                                           $heating_system
 * @property string|null                                                                           $view
 * @property string|null                                                                           $frame
 * @property string|null                                                                           $floors
 * @property string|null                                                                           $slope
 * @property string|null                                                                           $door
 * @property string|null                                                                           $closet
 * @property string|null                                                                           $garden
 * @property string|null                                                                           $swimming_pool
 * @property string|null                                                                           $access_from
 * @property string|null                                                                           $parking
 * @property int|null                                                                              $penthouse
 * @property int|null                                                                              $average_service_charges
 * @property int|null                                                                              $service_charges
 * @property int|null                                                                              $pet_allowance
 * @property string|null                                                                           $status
 * @property string|null                                                                           $energy_class
 * @property string|null                                                                           $orientation
 * @property string|null                                                                           $spatial_planning
 * @property string|null                                                                           $master_rooms
 * @property int|null                                                                              $plot_area_size
 * @property string|null                                                                           $renovation_year
 * @property int|null                                                                              $Balconies_sqm
 * @property string|null                                                                           $transportation
 * @property int|null                                                                              $distance_from_sea
 * @property int|null                                                                              $distance_from_city
 * @property int|null                                                                              $distance_from_village
 * @property int|null                                                                              $distance_from_airport
 * @property int|null                                                                              $distance_from_metro
 * @property int|null                                                                              $suitable_for_students
 * @property int|null                                                                              $luxury
 * @property int|null                                                                              $main_street
 * @property int|null                                                                              $elevator
 * @property int|null                                                                              $special_needs_access
 * @property int|null                                                                              $storage_room
 * @property int|null                                                                              $fireplace
 * @property int|null                                                                              $ac
 * @property int|null                                                                              $security_alarm
 * @property int|null                                                                              $furnished
 * @property int|null                                                                              $electrical_devices
 * @property int|null                                                                              $satellite_antenna
 * @property int|null                                                                              $night_electricity
 * @property int|null                                                                              $preserved
 * @property int|null                                                                              $neoclassical
 * @property int|null                                                                              $whole_floor_apartment
 * @property int|null                                                                              $bright
 * @property int|null                                                                              $double_glazing
 * @property int|null                                                                              $window_screens
 * @property int|null                                                                              $internal_staircase
 * @property int|null                                                                              $awnings
 * @property int|null                                                                              $bbq
 * @property int|null                                                                              $attic
 * @property int|null                                                                              $playroom
 * @property int|null                                                                              $solar
 * @property int|null                                                                              $painted
 * @property int|null                                                                              $entrance_steps
 * @property int|null                                                                              $commercial_property
 * @property int|null                                                                              $panoramic
 * @property int|null                                                                              $coastal
 * @property int|null                                                                              $investment
 * @property int|null                                                                              $spaces
 * @property int|null                                                                              $showcase_length
 * @property int|null                                                                              $facade
 * @property int|null                                                                              $equipped
 * @property int|null                                                                              $suspended_celling
 * @property int|null                                                                              $structured_cabling
 * @property int|null                                                                              $security_shutters
 * @property int|null                                                                              $health_care_use
 * @property int|null                                                                              $cargo_lift
 * @property int|null                                                                              $loading_ramb
 * @property int|null                                                                              $yard
 * @property int|null                                                                              $structure_factor
 * @property int|null                                                                              $coverage_factor
 * @property int|null                                                                              $allowed_height
 * @property string|null                                                                           $within_out_of_the_city_plan
 * @property string|null                                                                           $land_use
 * @property int|null                                                                              $width
 * @property int|null                                                                              $construction_exist_on
 * @property int|null                                                                              $conforming
 * @property int|null                                                                              $buildable
 * @property int|null                                                                              $with_building_permit
 * @property int|null                                                                              $fenced
 * @property int|null                                                                              $with_drilling
 * @property int|null                                                                              $with_water_supply
 * @property int|null                                                                              $with_power_supply
 * @property int|null                                                                              $with_natural_gas_supply
 * @property int|null                                                                              $with_telephone_line
 * @property int|null                                                                              $with_optical_line
 * @property int|null                                                                              $corner_property
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Actions\ActionEvent[]       $actions
 * @property-read int|null                                                                         $actions_count
 * @property-read \App\Models\Broker                                                               $broker
 * @property-read \App\Models\User|null                                                            $creator
 * @property-read \App\Models\User|null                                                            $deleter
 * @property-read \App\Models\District                                                             $district
 * @property-read string                                                                           $full_location_name
 * @property-read string[]                                                                         $photo_urls
 * @property-read string                                                                           $title
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|Media[] $media
 * @property-read int|null                                                                         $media_count
 * @property-read \App\Models\Municipality                                                         $municipality
 * @property-read \Illuminate\Database\Eloquent\Collection|\DigitalCloud\ModelNotes\Note[]         $notes
 * @property-read int|null                                                                         $notes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PropertyApplication[]       $property_applications
 * @property-read int|null                                                                         $property_applications_count
 * @property-read \App\Models\PropertyCategory                                                     $property_category
 * @property-read \App\Models\User|null                                                            $updater
 * @property-read \App\Models\Vicinity                                                             $vicinity
 * @method static Builder|Property byActiveBrokers($language = null)
 * @method static Builder|Property createdBy($userId)
 * @method static Builder|Property forLanguage($language)
 * @method static Builder|Property newModelQuery()
 * @method static Builder|Property newQuery()
 * @method static Builder|Property query()
 * @method static Builder|Property updatedBy($userId)
 * @method static Builder|Property whereAc($value)
 * @method static Builder|Property whereAccessFrom($value)
 * @method static Builder|Property whereAllowedHeight($value)
 * @method static Builder|Property whereAttic($value)
 * @method static Builder|Property whereAvailableFrom($value)
 * @method static Builder|Property whereAverageServiceCharges($value)
 * @method static Builder|Property whereAwnings($value)
 * @method static Builder|Property whereBalconiesSqm($value)
 * @method static Builder|Property whereBathroom($value)
 * @method static Builder|Property whereBbq($value)
 * @method static Builder|Property whereBedrooms($value)
 * @method static Builder|Property whereBright($value)
 * @method static Builder|Property whereBrokerId($value)
 * @method static Builder|Property whereBuildable($value)
 * @method static Builder|Property whereCargoLift($value)
 * @method static Builder|Property whereCloset($value)
 * @method static Builder|Property whereCoastal($value)
 * @method static Builder|Property whereCommercialProperty($value)
 * @method static Builder|Property whereConforming($value)
 * @method static Builder|Property whereConstructionExistOn($value)
 * @method static Builder|Property whereConstructionYear($value)
 * @method static Builder|Property whereCornerProperty($value)
 * @method static Builder|Property whereCoverageFactor($value)
 * @method static Builder|Property whereCreatedAt($value)
 * @method static Builder|Property whereCreatedBy($value)
 * @method static Builder|Property whereDeletedAt($value)
 * @method static Builder|Property whereDeletedBy($value)
 * @method static Builder|Property whereDescription($value)
 * @method static Builder|Property whereDisplayedAddress($value)
 * @method static Builder|Property whereDistanceFromAirport($value)
 * @method static Builder|Property whereDistanceFromCity($value)
 * @method static Builder|Property whereDistanceFromMetro($value)
 * @method static Builder|Property whereDistanceFromSea($value)
 * @method static Builder|Property whereDistanceFromVillage($value)
 * @method static Builder|Property whereDistrictId($value)
 * @method static Builder|Property whereDoor($value)
 * @method static Builder|Property whereDoubleGlazing($value)
 * @method static Builder|Property whereElectricalDevices($value)
 * @method static Builder|Property whereElevator($value)
 * @method static Builder|Property whereEnergyClass($value)
 * @method static Builder|Property whereEntranceSteps($value)
 * @method static Builder|Property whereEquipped($value)
 * @method static Builder|Property whereFacade($value)
 * @method static Builder|Property whereFenced($value)
 * @method static Builder|Property whereFireplace($value)
 * @method static Builder|Property whereFloor($value)
 * @method static Builder|Property whereFloors($value)
 * @method static Builder|Property whereFrame($value)
 * @method static Builder|Property whereFurnished($value)
 * @method static Builder|Property whereGarden($value)
 * @method static Builder|Property whereHealthCareUse($value)
 * @method static Builder|Property whereHeatingSystem($value)
 * @method static Builder|Property whereId($value)
 * @method static Builder|Property whereInternalStaircase($value)
 * @method static Builder|Property whereInvestment($value)
 * @method static Builder|Property whereKitchenRoom($value)
 * @method static Builder|Property whereLandUse($value)
 * @method static Builder|Property whereLevel($value)
 * @method static Builder|Property whereLivingRoom($value)
 * @method static Builder|Property whereLoadingRamb($value)
 * @method static Builder|Property whereLuxury($value)
 * @method static Builder|Property whereMainStreet($value)
 * @method static Builder|Property whereMasterRooms($value)
 * @method static Builder|Property whereMunicipalityId($value)
 * @method static Builder|Property whereNeoclassical($value)
 * @method static Builder|Property whereNightElectricity($value)
 * @method static Builder|Property whereOrientation($value)
 * @method static Builder|Property wherePainted($value)
 * @method static Builder|Property wherePanoramic($value)
 * @method static Builder|Property whereParking($value)
 * @method static Builder|Property wherePenthouse($value)
 * @method static Builder|Property wherePetAllowance($value)
 * @method static Builder|Property wherePlayroom($value)
 * @method static Builder|Property wherePlotAreaSize($value)
 * @method static Builder|Property wherePostcode($value)
 * @method static Builder|Property wherePreserved($value)
 * @method static Builder|Property wherePrice($value)
 * @method static Builder|Property wherePropertyCategoryId($value)
 * @method static Builder|Property whereRealAddress($value)
 * @method static Builder|Property whereRenovationYear($value)
 * @method static Builder|Property whereSatelliteAntenna($value)
 * @method static Builder|Property whereSecurityAlarm($value)
 * @method static Builder|Property whereSecurityShutters($value)
 * @method static Builder|Property whereServiceCharges($value)
 * @method static Builder|Property whereShowcaseLength($value)
 * @method static Builder|Property whereSlope($value)
 * @method static Builder|Property whereSolar($value)
 * @method static Builder|Property whereSpaces($value)
 * @method static Builder|Property whereSpatialPlanning($value)
 * @method static Builder|Property whereSpecialNeedsAccess($value)
 * @method static Builder|Property whereSqm($value)
 * @method static Builder|Property whereStatus($value)
 * @method static Builder|Property whereStorageRoom($value)
 * @method static Builder|Property whereStructureFactor($value)
 * @method static Builder|Property whereStructuredCabling($value)
 * @method static Builder|Property whereSubcategory($value)
 * @method static Builder|Property whereSubregion($value)
 * @method static Builder|Property whereSuitableForStudents($value)
 * @method static Builder|Property whereSupplyType($value)
 * @method static Builder|Property whereSuspendedCelling($value)
 * @method static Builder|Property whereSwimmingPool($value)
 * @method static Builder|Property whereTransportation($value)
 * @method static Builder|Property whereUpdatedAt($value)
 * @method static Builder|Property whereUpdatedBy($value)
 * @method static Builder|Property whereVicinityId($value)
 * @method static Builder|Property whereView($value)
 * @method static Builder|Property whereWc($value)
 * @method static Builder|Property whereWholeFloorApartment($value)
 * @method static Builder|Property whereWidth($value)
 * @method static Builder|Property whereWindowScreens($value)
 * @method static Builder|Property whereWithBuildingPermit($value)
 * @method static Builder|Property whereWithDrilling($value)
 * @method static Builder|Property whereWithNaturalGasSupply($value)
 * @method static Builder|Property whereWithOpticalLine($value)
 * @method static Builder|Property whereWithPowerSupply($value)
 * @method static Builder|Property whereWithTelephoneLine($value)
 * @method static Builder|Property whereWithWaterSupply($value)
 * @method static Builder|Property whereWithinOutOfTheCityPlan($value)
 * @method static Builder|Property whereYard($value)
 * @method static Builder|Property whereYoutube($value)
 * @mixin \Eloquent
 */
class Property extends Model implements HasMedia
{
    use ModelTraits;
    use HasFactory;
    use InteractsWithMedia;

    protected $hidden = ['media'];

    public static $_casts = [];
    protected $casts = [];
    public static $_dates = [];
    protected $dates = [];
    protected $fillable = [];

    /**
     * @param Builder                  $query
     * @param Language|string|int|null $language
     * @return Builder
     */
    public function scopeByActiveBrokers(Builder $query, $language = null)
    {
        return $query->whereHas('broker', function (Builder $q) use ($language) {
            return $q->scopes(['active'])
                ->when($language, function (Builder $q, $l) {
                    return $q->scopes(['WithLanguage' => [$l]]);
                });
        });
    }

    /**
     * @param Builder             $query
     * @param Language|string|int $language
     * @return Builder
     */
    public function scopeForLanguage(Builder $query, $language)
    {
        return $query->whereHas('broker', function (Builder $q) use ($language) {
            return $q->scopes(['WithLanguage' => [$language]]);
        });
    }

    public function __construct(array $attributes = [])
    {
        $this->dates = &self::$_dates;
        $this->casts = &self::$_casts;
        parent::__construct($attributes);
    }

    /**
     * @param null|string $language
     * @return string
     */
    public function getFullLocationName($language = null)
    {
        return "{$this->vicinity->getName($language)}, {$this->municipality->getName($language)}, {$this->district->getName($language)}";
    }

    /**
     * @return string
     */
    public function getFullLocationNameAttribute()
    {
        return $this->getFullLocationName(app()->getLocale());
    }

    /**
     * @param null|string $language
     * @return string
     */
    public function getTitle($language = null)
    {
        $name = '';
        if ($this->subcategory)
            $name = humanize_attr($this->subcategory) . ' ';
        if ($this->sqm)
            $name .= "{$this->sqm} " . __('sqm') . ', ';
        $name .= "{$this->district->getName($language)}, {$this->municipality->getName($language)}, € " . number_format($this->price, 0, ',', '.');
        return $name;
    }

    /**
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->getTitle(app()->getLocale());
    }

    /**
     * @return string[]
     */
    public function getPhotoUrlsAttribute()
    {
        return $this->getMedia('photos')->map->getFullUrl();
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('photos')
            ->useDisk(config('property.photos.disk'))
//            ->withResponsiveImages()//PAID
            ->acceptsFile(function (\Spatie\MediaLibrary\MediaCollections\File $file) {
                return starts_with($file->mimeType, 'image/');
            })
            ->registerMediaConversions(function (Media $media) {
                $media->setCustomProperty('path', config('property.photos.path'));
            });
    }

    public function broker()
    {
        return $this->belongsTo(Broker::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

    public function property_category()
    {
        return $this->belongsTo(PropertyCategory::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

    public function municipality()
    {
        return $this->belongsTo(Municipality::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

    public function vicinity()
    {
        return $this->belongsTo(Vicinity::class)->withoutGlobalScope(SoftDeletingScope::class);
    }

    public function property_applications()
    {
        return $this->hasMany(PropertyApplication::class);
    }
}
