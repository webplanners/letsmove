<?php

namespace App\Notifications;

use App\Models\Broker;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Laravel\Nova\Nova;

class WelcomeBroker extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param Broker $broker
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($broker)
    {
        return (new MailMessage)
            ->subject(__("Welcome") . " {$broker->name}")
            ->line(__('Welcome to LetsMove.gr'))
            ->action(__('Buy Subscription'), url(Nova::path() . "/resources/brokers/{$broker->id}"))
            ->line(__('Thank you for choosing us!'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
