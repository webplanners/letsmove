<?php

namespace App\Notifications;

use App\Models\PropertyApplication;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Laravel\Nova\Nova;

class PropertyApplicationNotification extends Notification
{
    use Queueable;

    /**
     * @var PropertyApplication
     */
    public $property_application;

    public function __construct(PropertyApplication $pa)
    {
        $this->property_application = $pa;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('Property Application for ') . $this->property_application->property->title)
            ->line(__('Property application from :name (:contact) at :time', [
                'name'    => $this->property_application->name,
                'time'    => $this->property_application->created_at->toDateTimeString(),
                'contact' => implode(', ', array_filter([$this->property_application->email, $this->property_application->phone])),
            ]))
            ->when($this->property_application->negotiation_price, function (MailMessage $m, $price) {
                $m->line(__('Negotiation Price') . ": $price");
            })
            ->line('"' . $this->property_application->message . '"')
            ->action(__('View Property Application'), url(Nova::path() . "/resources/property-applications/{$this->property_application->id}"))
            ->line(__('Thank you for choosing us!'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
