<?php

namespace App\Policies;

use App\Models\Broker;
use App\Models\Language;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class LanguagePolicy extends BasePolicy
{
    public function attachBroker(User $user, Language $language, Broker $broker)
    {
        return $broker->getRemainingLanguages() > 0 ?: Response::deny("Broker has reached purchased languages limit.");
    }
}
