<?php

namespace App\Policies;

use App\Models\Broker;
use App\Models\Language;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class BrokerPolicy extends BasePolicy
{
    /**
     * @param User   $user
     * @param Broker $model
     * @return array|bool|mixed
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $this->sameBroker($user, $model);
    }

    public function create(User $user)
    {
        return parent::create($user) || empty($user->broker_id);
    }

    /**
     * @param User   $user
     * @param Broker $model
     * @return array|bool|mixed
     */
    public function update(User $user, $model)
    {
        return parent::update($user, $model) || $this->sameBroker($user, $model);
    }

    public function delete(User $user, $model)
    {
        return parent::delete($user, $model);
    }

    public function restore(User $user, $model)
    {
        return parent::restore($user, $model) || $this->sameBroker($user, $model);
    }

    public function attachAnyLanguage(User $user, Broker $broker)
    {
        return $broker->getRemainingLanguages() > 0 ?: Response::deny("Reached purchased languages limit.");
    }

    public function attachLanguage(User $user, Broker $broker, Language $language)
    {
        if ($language->default) {
            return Response::deny("Language is default.");
        }
        if ($broker->languages->contains($language)) {
            return Response::deny("Already added this language.");
        }
        return $broker->getRemainingLanguages() > 0 ?: Response::deny("Reached purchased languages limit.");
    }

}
