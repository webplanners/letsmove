<?php

namespace App\Policies;

use App\Models\User;

class ActionEventPolicy extends BasePolicy
{
//    public function viewAny(User $user, $model)
//    {
//        return $this->can($user, "viewAny {$this->model}", $model);
//    }

    /**
     * @param User $user
     * @param      $targetModel - It is the model the ActionEvent is applied on and not the particular ActionEvent.
     * @return bool
     */
    public function view(User $user, $targetModel)
    {
        return $this->can($user, "viewAny {$this->model}", $targetModel);
    }

    public function create(User $user)
    {
        return false;
    }

    public function update(User $user, $model)
    {
        return false;
    }

    public function delete(User $user, $model)
    {
        return false;
    }

    public function restore(User $user, $model)
    {
        return false;
    }
}
