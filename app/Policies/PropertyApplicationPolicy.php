<?php

namespace App\Policies;

use App\Models\PropertyApplication;
use App\Models\User;

class PropertyApplicationPolicy extends BasePolicy
{
    /**
     * @param User                $user
     * @param PropertyApplication $model
     * @return array|bool|mixed
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $this->sameBroker($user, $model);
    }

    public function delete(User $user, $model)
    {
        return parent::delete($user, $model) || $this->sameBroker($user, $model);
    }

    public function restore(User $user, $model)
    {
        return parent::restore($user, $model) || $this->sameBroker($user, $model);
    }
}
