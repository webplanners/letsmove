<?php

namespace App\Policies;

use App\Models\User;

class UserPolicy extends BasePolicy
{

    /**
     * @param User $user
     * @param User $model
     * @return array|bool|mixed
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $model->id == $user->id || $this->sameBroker($user, $model);
    }

    /**
     * @param User $user
     * @param User $model
     * @return array|bool|mixed
     */
    public function update(User $user, $model)
    {
        return (parent::update($user, $model) && !$model->isCritical()) || $model->id == $user->id;
    }

    /**
     * @param User $user
     * @param User $model
     * @return array|bool|mixed
     */
    public function delete(User $user, $model)
    {
        return parent::delete($user, $model) && !$model->isCritical();
    }

}
