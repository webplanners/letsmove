<?php

namespace App\Policies;

use App\Models\Property;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class PropertyPolicy extends BasePolicy
{
    public function create(User $user)
    {
        return (parent::create($user) || $user->broker->getRemainingProperties() > 0) ?: Response::deny("Reached purchased properties limit.");
    }

    /**
     * @param User     $user
     * @param Property $model
     * @return bool
     */
    public function update(User $user, $model)
    {
        return parent::update($user, $model) || $this->sameBroker($user, $model);
    }

    /**
     * @param User     $user
     * @param Property $model
     * @return array|bool|mixed
     */
    public function view(User $user, $model)
    {
        return parent::view($user, $model) || $this->sameBroker($user, $model);
    }

    public function delete(User $user, $model)
    {
        return parent::delete($user, $model) || $this->sameBroker($user, $model);
    }

    /**
     * @param User     $user
     * @param Property $property
     * @return array|bool|mixed
     */
    public function restore(User $user, $property)
    {
        return ((parent::restore($user, $property) || $this->sameBroker($user, $property)) && $property->broker->getRemainingProperties() > 0) ?: Response::deny("Purchased properties limit has been reached.");
    }

}
