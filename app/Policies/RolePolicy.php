<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;

class RolePolicy extends BasePolicy
{
    /**
     * @param User $user
     * @param Role $model
     * @return bool
     */
    public function update(User $user, $model)
    {
        return !$model->is_super_admin && parent::update($user, $model);
    }

}
