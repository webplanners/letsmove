<?php
/**
 * helpers.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 23/6/20 9:10 μ.μ.
 */

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Telescope\Telescope;

if (!function_exists('humanize')) {

    /**
     * @param object|string $object Class or object to translate
     * @return string
     */
    function humanize($object)
    {
        return __(title_case(snake_case(class_basename($object), ' ')));
    }
}

if (!function_exists('humanize_attr')) {

    /**
     * @param string $attr Attribute in snake_case to translate
     * @return string
     */
    function humanize_attr($attr)
    {
        return __(ucwords(str_replace(['-', '_'], ' ', $attr)));
    }
}

if (!function_exists('is_assoc')) {
    /**
     * @param array $arr
     * @return bool - if the array is associative
     */
    function is_assoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}

if (!function_exists('valid_file')) {
    /**
     * @param        $file
     * @param string $disk Empty disk means no disk but the file param has complete path.
     * @return bool tru if file parameter is filled, file exists and its size is larger than 1
     */
    function valid_file($file, $disk = null)
    {
        if (empty($file))
            return false;
        if (!empty($disk))
            $file = \Illuminate\Support\Facades\Storage::disk($disk)->path($file);
        return file_exists($file) && filesize($file);
    }
}

if (!function_exists('while_logged_in')) {
    /**
     * Login as the given user if there is not one already logged in, execute the callback and logout only if a login happened from this function.
     * @param callable $callback
     * @param User     $user As which user to login
     * @return mixed Result of callback
     */
    function while_logged_in(callable $callback, User $user)
    {
        if (!($wasLogged = Auth::check()))
            Auth::login($user);
        $result = $callback();
        if (!$wasLogged)
            Auth::logout();
        return $result;
    }
}

if (!function_exists('log_step')) {
    /**
     * Log current step of process for debugging performance.
     * @param string $step
     * @param string $level
     */
    function log_step($step, $level = 'debug')
    {
        static $last = LARAVEL_START;
        $now = microtime(true);
        $previous = floor(($now - $last) * 1000);
        $total = floor(($now - LARAVEL_START) * 1000);
        Log::log($level, "$step +{$previous}ms - {$total}ms");
        $last = $now;
    }
}

if (!function_exists('public_url')) {
    /**
     * @param string $url Replaces the locally defined app url (usually http://192.168.1.77) with the actual url of this request (e.g. https://79.129.22.252)
     * @return string
     */
    function public_url($url)
    {
        return str_replace(config('app.url'), request()->getSchemeAndHttpHost(), $url);
    }
}

if (!function_exists('sign')) {
    /**
     * Return the sign of the given number.
     * If number is 0 returns the zero parameter.
     * @param float|int $n
     * @param int       $zero
     * @return float|int
     */
    function sign($n, $zero = 0)
    {
        return $n ? ($n > 0 ? 1 : -1) : $zero;
    }
}

if (!function_exists('filesize_formatted')) {
    /**
     * @param string|int|float $file For string path of file to calculate, otherwise number of bytes to format.
     * @param int              $decimals
     * @return string
     */
    function filesize_formatted($file, $decimals = 2)
    {
        $bytes = is_string($file) ? filesize($file) : $file;
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }
}

if (!function_exists('parse_money')) {
    /**
     * @param null|string|int|float $money Money from json
     * @return string
     */
    function parse_money($money)
    {
        return is_numeric($money) ? $money / 100 : $money;
    }
}

if (!function_exists('stripe')) {
    /**
     * @return \App\Stripe\StripeManager
     */
    function stripe()
    {
        return new \App\Stripe\StripeManager();
    }
}

if (!function_exists('flatten_permissions_map')) {
    /**
     * @param array|\Illuminate\Support\Collection $permissions_map
     * @return \Illuminate\Support\Collection|array
     */
    function flatten_permissions_map($permissions_map)
    {
//        $permissions_map = [
//            'view'         => [
//                'District',
//                'Municipality',
//                'Vicinity',
//            ],
//            'create'       => [
//            ],
//            'Note'         => [
//                'create',
//                'view',
//            ],
//            'District'     => '*',
//            'Municipality' => ['*'],
//            '*'            => [
//                'Property',
//                'Vicinity',
//            ],
//            '*'            => 'Property',
//            'run commands',
//        ];
        $permissions = [];
        foreach ($permissions_map as $action => $models) {
            if ($action === '*') {
                $actions = \App\Models\Permission::ACTIONS;
                $models = array_wrap($models);
                foreach ($actions as $action)
                    foreach ($models as $model)
                        $permissions[] = "$action $model";
                continue;
            }
            if ($models === '*' || $models === ['*'])
                $models = \App\Models\Permission::ACTIONS;
            if (is_array($models)) {
                if (in_array($action, \App\Models\Permission::ACTIONS)) {
                    foreach ($models as $model) {
                        $permissions[] = "$action $model";
                    }
                } else {
                    list($actions, $model) = [$models, $action];
                    foreach ($actions as $action) {
                        throw_unless(in_array($action, \App\Models\Permission::ACTIONS), \RuntimeException::class, "Invalid Permission action '$action $model'");
                        $permissions[] = "$action $model";
                    }
                }
            } else {
                $permissions[] = $models;
            }
        }
        return array_unique($permissions);
    }
}

/**
 * @param array data array
 * @param string[]|string[][] $keys   Array of new keys pointing to existing columns of the passed array.
 *                                    Multiple values may be specified for the same key as fallback.
 * @param string[]|null       $mapper Optionally returns the mapped keys to columns
 * @return array
 */
function array_build_map($array, $keys, &$mapper = null)
{
    if (empty($array) || empty($keys))
        return [];

    // setup keys mapper
    $mapper = [];
    $headers = array_keys(head($array));
    foreach ($keys as $key => $columns) {
        if (is_array($columns)) {
            foreach ($columns as $column) {
                foreach ($headers as $header) {
                    if (Str::is($column, $header)) {
                        $mapper[$key] = $header;
                        break;
                    }
                }
                if (!empty($mapper[$key]))
                    break;
            }
        } else {
            $mapper[$key] = $columns;
        }
        throw_if(empty($mapper[$key]), \App\Exceptions\InvalidDataException::class, "No column found for key '$key'");
    }

    // parse array into transformed
    $data = [];
    foreach ($array as $row) {
        $t = [];
        foreach ($mapper as $key => $column) {
            $t[$key] = $row[$column];
        }
        $data[] = $t;
    }

    return $data;
}

/**
 * @return string|null
 */
function telescope_exception()
{
    // \Facade\Ignition\ErrorPage\ErrorPageViewModel::telescopeUrl
    try {
        if (!class_exists(Telescope::class)) {
            return null;
        }

        if (!count(Telescope::$entriesQueue)) {
            return null;
        }

        $telescopeEntry = collect(Telescope::$entriesQueue)->first(function (\Laravel\Telescope\IncomingEntry $entry) {
            return $entry->type == 'exception';
        });

        if (is_null($telescopeEntry)) {
            return null;
        }

        $telescopeEntryId = (string)$telescopeEntry->uuid;
        return $telescopeEntryId;
        //return url(action([Laravel\Telescope\Http\Controllers\HomeController::class, 'index']) . "/exceptions/{$telescopeEntryId}");
    } catch (Exception $exception) {
        report($exception);
        return null;
    }
}

/**
 * @param \Illuminate\Database\Eloquent\Model|array|mixed $model
 * @param string                                          $attr
 */
function model_id($model, $attr = null)
{
    if ($model instanceof \Illuminate\Database\Eloquent\Model) {
        return $attr ? $model->{$attr} : $model->getKey();
    }
    if (is_array($model)) {
        return $model[$attr ?? 'id'];
    }
    return $model;
}
