<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BelongsToBroker
{
    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        abort_if(empty($request->user()->broker), 412, 'User must belong to a broker.');
        return $next($request);
    }
}
