<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageOptimization
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // from Spatie\LaravelImageOptimizer\Middlewares\OptimizeImages::handle
        collect($request->allFiles())
            ->flatten()
            ->filter(function (UploadedFile $file) {
                return $file->isValid() && starts_with($file->getMimeType(), 'image/');
            })
            ->each(function (UploadedFile $file) {
                ImageOptimizer::optimize($file->getPathname());
            });

        return $next($request);
    }
}
