<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BrokerSession
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (empty($roles = session('roles'))) {
            $user = $request->user();
            $roles = [
                'broker' => $broker = $user->can('broker'),
                'admin'  => $admin = $user->can('admin'),
            ];
            session(['roles' => $roles]);
        }
        \App\Nova\Resource::$is_broker = \App\Nova\Actions\BaseAction::$is_broker = $roles['broker'];
        $request->attributes->set('is_broker', $roles['broker']);
        \App\Nova\Resource::$is_admin = \App\Nova\Actions\BaseAction::$is_admin = $roles['admin'];
        $request->attributes->set('is_admin', $roles['admin']);
        \App\Nova\Resource::$broker_only = \App\Nova\Actions\BaseAction::$broker_only = $roles['broker'] && !$roles['admin'];
        $request->attributes->set('broker_only', $roles['broker'] && !$roles['admin']);
        return $next($request);
    }

}
