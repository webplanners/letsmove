<?php

namespace App\Http\Middleware;

use App\Models\Language;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($l = session('language.code')) {
            App::setLocale($l);
        } else {
            $code = $request->getPreferredLanguage(Language::all()->pluck('code')->all());
            $l = Language::all()->firstWhere('code', $code) ?? Language::default();
            session(['language' => $l]);
            App::setLocale($l->code);
        }
        return $next($request);
    }
}
