<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'stripe/webhook',
    ];

    /**
     * @inheritDoc
     */
    protected function getTokenFromRequest($request)
    {
        $token = parent::getTokenFromRequest($request);
        if ($request->attributes->getBoolean('sanctum') && empty($token)) {
            return $request->cookie('XSRF-TOKEN');
        }
        return $token;
    }
}
