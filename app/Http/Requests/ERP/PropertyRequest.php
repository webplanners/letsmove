<?php

namespace App\Http\Requests\ERP;

use App\Models\Property;
use App\Models\PropertyCategory;
use App\Models\Vicinity;
use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;

    public PropertyCategory $pc;

    protected function prepareForValidation()
    {
        // If not found or not provided an error will be thrown while validating.
        $this->pc = PropertyCategory::find($this->input('property_category'));
    }

    public function authorize()
    {
        return true; // authorized in controller
    }

    public function rules()
    {
        $basic = [
            'vicinity'          => 'required|exists:' . \App\Models\Vicinity::class . ',id',
            'property_category' => 'required|exists:' . \App\Models\PropertyCategory::class . ',id',
            'youtube'           => 'nullable|string',
            'description'       => 'nullable|string|max:700',
            'retain_photos'     => 'nullable|boolean',
            'photos'            => 'nullable|array|max:' . config('property.photos.count'),
            'photos.*'          => 'image|max:' . config('property.photos.file_size'),
        ];
        if (empty($this->pc)) {
            return $basic;
        }
        return $this->pc->getValidations()->merge($basic)->all();
    }

    public function attributes()
    {
        $basic = [
            'vicinity'          => __('Vicinity'),
            'property_category' => __('Property Category'),
            'photos'            => __('Photos'),
            'photos.*'          => __('Photo'),
            'description'       => __('Description'),
            'youtube'           => __('YouTube'),
        ];
        if (empty($this->pc)) {
            return $basic;
        }
        return $this->pc->listAttributeNames()->merge($basic)->all();
    }

    /**
     * @param Property|null $existingProperty
     * @return Property
     */
    public function buildProperty($existingProperty = null)
    {
        $p = $existingProperty ?? new Property();
        $p->description = $this->description;
        $p->youtube = $this->youtube;
        $vicinity = Vicinity::findOrFail($this->input('vicinity'));
        $p->vicinity()->associate($vicinity);
        $p->municipality()->associate($vicinity->municipality);
        $p->district()->associate($vicinity->district);
        $p->broker()->associate($this->user()->broker);
        $this->pc->fillProperty($p, $this->validated());
        if (!$this->boolean('retain_photos', true)) {
            $p->clearMediaCollection('photos');
        }
        if ($this->hasFile('photos')) {
//            $p->addMediaFromRequest('photos')->toMediaCollection('photos');
            $p->addMultipleMediaFromRequest(['photos'])
                ->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection('photos');
                });
        }
        return $p;
    }
}
