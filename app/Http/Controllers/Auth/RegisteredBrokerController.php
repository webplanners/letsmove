<?php

namespace App\Http\Controllers\Auth;

use App\Events\BrokerRegistered;
use App\Http\Controllers\Controller;
use App\Models\Broker;
use App\Models\Role;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisteredBrokerController extends Controller
{
    public function create()
    {
        return view('auth.register-broker');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed|\Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $attrs = $request->validate([
            'name'                           => 'required|string',
            'type'                           => 'required|string|in:' . implode(',', Broker::TYPES),
            'address'                        => 'required|string',
            'phone'                          => 'required|string',
            'vin'                            => 'required|digits:9|unique:brokers,vin',
            'commercial_registration_number' => 'nullable|digits:12|unique:brokers,commercial_registration_number',
        ]);
        $user = $request->user();
        $broker = $user->broker;
        abort_if(filled($broker), 401, 'User already registered broker.');
        DB::transaction(function () use ($attrs, $user, &$broker) {
            $broker = Broker::create($attrs);
            $user->broker()->associate($broker);
            $user->saveOrFail();
            $user->assignRole(Role::broker());
        });
        event(new BrokerRegistered($broker));
        if ($request->wantsJson()) {
            return $broker;
        }
        return redirect(RouteServiceProvider::HOME);
    }
}
