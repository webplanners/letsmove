<?php

namespace App\Http\Controllers;

use App\Events\PropertyApplicationReceived;
use App\Http\Resources\LanguageResource;
use App\Http\Resources\LocationResource;
use App\Http\Resources\PropertyCollection;
use App\Http\Resources\PropertyResource;
use App\Models\District;
use App\Models\Language;
use App\Models\Municipality;
use App\Models\Property;
use App\Models\PropertyApplication;
use App\Models\Vicinity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class PublicFrontEndController extends Controller
{

    public function getCurrentLanguage()
    {
        return new LanguageResource($this->currentLanguage());
    }

    public function setLanguage($code)
    {
        $l = Language::findByCode($code);
        session(['language' => $l]);
        return new LanguageResource($l);
    }

    /**
     * @return Language
     */
    protected function currentLanguage()
    {
        return session('language');
    }

    public function searchLocation(Request $request)
    {
        $request->validate([
            'name'  => 'required|min:3',
            'limit' => 'nullable|between:1,50',
        ]);
        $name = $request->input('name');
        $limit = $request->input('limit', 5);
        return LocationResource::collection(
            District::searchName($name)->limit($limit)->get()->append('type')
                ->concat(
                    Municipality::searchName($name)->limit($limit)->get()->append('type')
                )->concat(
                    Vicinity::searchName($name)->limit($limit)->get()->append('type')
                )
        );
    }

    public function searchProperty(Request $request)
    {
        $request->validate([
            'order_by'           => 'nullable|string',
            'order_by_direction' => 'nullable|string|in:asc,desc',
            'limit'              => 'nullable|integer|between:1,100',
            'district'           => 'required_without_all:municipality,vicinity|integer',
            'municipality'       => 'required_without_all:district,vicinity|integer',
            'vicinity'           => 'required_without_all:district,municipality|integer',
            'property_category'  => 'nullable|integer',
            'price_min'          => 'nullable|integer',
            'price_max'          => 'nullable|integer',
            'supply_type'        => 'nullable|string',
            'bedrooms_min'       => 'nullable|integer',
            'added_last_seconds' => 'nullable|integer',
        ]);
        return new PropertyCollection(
            Property::with('broker')
                ->scopes(['ByActiveBrokers' => [$this->currentLanguage()]])
                ->when($request->input('district'), function (Builder $query, $district_id) {
                    return $query->where('district_id', $district_id);
                })
                ->when($request->input('municipality'), function (Builder $query, $municipality) {
                    return $query->where('municipality_id', $municipality);
                })
                ->when($request->input('vicinity'), function (Builder $query, $vicinity) {
                    return $query->where('vicinity_id', $vicinity);
                })
                ->when($request->input('property_category'), function (Builder $query, $pc) {
                    return $query->where('property_category_id', $pc);
                })
                ->when($request->input('price_min'), function (Builder $query, $price_min) {
                    return $query->where('price', '>=', $price_min);
                })
                ->when($request->input('price_max'), function (Builder $query, $price_max) {
                    return $query->where('price', '<=', $price_max);
                })
                ->when($request->input('supply_type'), function (Builder $query, $supply_type) {
                    return $query->where('supply_type', $supply_type);
                })
                ->when($request->input('bedrooms_min'), function (Builder $query, $bedrooms_min) {
                    return $query->where('bedrooms_min', '>=', $bedrooms_min);
                })
                ->when($request->input('added_last_seconds'), function (Builder $query, $added_last) {
                    return $query->where('created_at', '>=', now()->subSeconds($added_last));
                })
                ->orderBy($request->input('order_by', 'created_at'), $request->input('order_by_direction', 'desc'))
                ->paginate($request->input('limit', 25))
        );
    }

    public function showProperty(Request $request, $id)
    {
        return new PropertyResource(
            Property::with('broker')
                ->scopes(['ByActiveBrokers' => [$this->currentLanguage()]])
                ->where('id', $id)->firstOrFail()
        );
    }

    public function storePropertyApplication(Request $request, Property $property)
    {
        $data = $request->validate([
            'name'              => 'required',
            'email'             => 'required_without:phone|nullable|email',
            'message'           => 'required',
            'phone'             => 'required_without:email|nullable',
            'negotiation_price' => 'nullable|numeric',
        ]);
        $data['broker_id'] = $property->broker_id;
        /** @var PropertyApplication $pa */
        $pa = $property->property_applications()->create($data);
        event(new PropertyApplicationReceived($pa));
        return $pa;
    }

}
