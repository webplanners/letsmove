<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\DistrictCollection;
use App\Models\District;

class DistrictController extends Controller
{
    public function index()
    {
        return new DistrictCollection(District::paginate(100));
    }

    public function show(District $district)
    {
        return $district;
    }
}
