<?php

namespace App\Http\Controllers\API\ERP;

use App\Http\Controllers\Controller;
use App\Http\Requests\ERP\PropertyRequest;
use App\Http\Resources\PropertyCollection;
use App\Http\Resources\PropertyResource;
use App\Models\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Property::class, 'property');
//        $this->middleware('image_optimization')->only(['store', 'update']); //PAID ImageOptimization
    }

    public function index(Request $request)
    {
        $q = $request->user()->broker->properties();
        $q = $request->input('sort') == 'oldest' ? $q->oldest() : $q->latest();
        return $request->has('count')
            ? $q->count()
            : new PropertyCollection($q->paginate(100));
    }

    public function store(PropertyRequest $request)
    {
        return new PropertyResource(tap($request->buildProperty())->saveOrFail());
    }

    public function show(Property $property)
    {
        return new PropertyResource($property);
    }

    public function update(PropertyRequest $request, Property $property)
    {
        return new PropertyResource(tap($request->buildProperty($property))->saveOrFail());
    }

    public function destroy(Property $property)
    {
        return new PropertyResource(tap($property)->delete());
    }

    public function restore($property_id)
    {
        $property = Property::onlyTrashed()->findOrFail($property_id);
        $this->authorize('restore', $property);
        return new PropertyResource(tap($property)->restore());
    }
}
