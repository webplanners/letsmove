<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\MunicipalityCollection;
use App\Models\Municipality;

class MunicipalityController extends Controller
{
    public function index()
    {
        return new MunicipalityCollection(Municipality::paginate(100));
    }

    public function show(Municipality $municipality)
    {
        return $municipality;
    }
}
