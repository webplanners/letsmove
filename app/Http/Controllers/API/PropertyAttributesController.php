<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\PropertyCategory;

class PropertyAttributesController extends Controller
{
    public function byCategory()
    {
        return PropertyCategory::all()->load('property_attributes');
    }

    public function forCategory($category)
    {
        return PropertyCategory::orWhere(['id' => $category, 'code' => $category])->firstOrFail()->property_attributes;
    }
}
