<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BrokerController extends Controller
{
    public function current(Request $request)
    {
        return $request->user()->broker;
    }
}
