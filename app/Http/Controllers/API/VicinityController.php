<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\VicinityCollection;
use App\Models\Vicinity;

class VicinityController extends Controller
{
    public function index()
    {
        return new VicinityCollection(Vicinity::paginate(100));
    }

    public function show(Vicinity $vicinity)
    {
        return $vicinity;
    }
}
