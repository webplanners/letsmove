<?php

namespace App\Http\Controllers;

use App\Models\Broker;
use App\Models\Subscription;
use App\Models\SubscriptionPlan;
use App\Stripe\StripeManager;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Cashier\Http\Controllers\WebhookController;

class StripeWebhookController extends WebhookController
{

    public function handleProductCreated($payload)
    {
        $data = $payload['data']['object'];
        try {
            $subscription_plan = SubscriptionPlan::newModelInstance()->syncFromData($data);
        } catch (\Exception $e) {
            report($e);
            return $this->error($e->getMessage());
        }
        $subscription_plan->refresh();
        return ['message' => "Subscription #{$subscription_plan->id} created."];
    }

    public function handleProductUpdated($payload)
    {
        $data = $payload['data']['object'];
        $subscription_plan = SubscriptionPlan::firstOrNew(['stripe_product_id' => $data['id']]);
        $subscription_plan->syncFromData($data);
        return $subscription_plan->deleted_at
            ? $this->successMethod("Subscription #{$subscription_plan->id} deleted")
            : $this->successMethod("Subscription #{$subscription_plan->id} updated.");
    }

    public function handleProductDeleted($payload)
    {
        $plan = $payload['data']['object'];
        abort_if($plan['metadata']['product'] != StripeManager::SUBSCRIPTION_PLAN_PRODUCT, $this->error('Product is not a subscription plan. Discarding deletion.'));
        $subscription_plan = SubscriptionPlan::findByStripeId($plan['id']);
        $subscription_plan->delete();
        return ['message' => "Subscription #{$subscription_plan->id} deleted."];
    }

    public function handlePriceCreated($payload)
    {
        try {
            $s = tap(SubscriptionPlan::findByStripeId($product_id = $payload['data']['object']['product'])->updatePrices())->saveOrFail();
        } catch (ModelNotFoundException $e) {
            report($e);
            return $this->error("Subscription $product_id not found for price creation.");
        }
        return ['message' => "Subscription #{$s->id} updated for created price."];
    }

    public function handlePriceUpdated($payload)
    {
        try {
            $s = tap(SubscriptionPlan::findByStripeId($product_id = $payload['data']['object']['product'])->updatePrices())->saveOrFail();
        } catch (ModelNotFoundException $e) {
            report($e);
            return $this->error("Subscription $product_id not found for price update.");
        }
        return ['message' => "Subscription #{$s->id} updated for updated price."];
    }

    public function handlePriceDeleted($payload)
    {
        try {
            $s = tap(SubscriptionPlan::findByStripeId($product_id = $payload['data']['object']['product'])->updatePrices())->saveOrFail();
        } catch (ModelNotFoundException $e) {
            report($e);
            return $this->error("Subscription $product_id not found for price deletion.");
        }
        return ['message' => "Subscription #{$s->id} updated for deleted price."];
    }

    /**
     * @param array $payload
     * @return mixed
     * @throws \Laravel\Cashier\Exceptions\PaymentActionRequired
     * @throws \Laravel\Cashier\Exceptions\PaymentFailure
     */
    public function handleCustomerSubscriptionCreated($payload)
    {
        // taken from \Laravel\Cashier\SubscriptionBuilder::create
        // \Laravel\Cashier\Http\Controllers\WebhookController::handleCustomerSubscriptionCreated
        $data = $payload['data']['object'];
        $broker = $this->getUserByStripeId($data['customer']);
        if (!$broker) {
            return $this->error("Broker {$data['customer']} not found.");
        }
        $broker->updateDefaultPaymentMethodFromStripe();
        if ($existingSub = $broker->subscriptions()->where(['stripe_id' => $data['id']])->first()) {
            return $this->error("Subscription {$existingSub->stripe_id} already exists #{$existingSub->id}");
        }

        $subscription = Subscription::forBroker($broker)->syncFromData($data);

        return ['message' => "Created subscription #{$subscription->id}."];
    }

    protected function handleCustomerSubscriptionUpdated(array $payload)
    {
        return parent::handleCustomerSubscriptionUpdated($payload); // TODO: remove properties over listings limit
    }

    protected function handleCustomerSubscriptionDeleted(array $payload)
    {
        parent::handleCustomerSubscriptionDeleted($payload);
        if ($broker = $this->getUserByStripeId($payload['data']['object']['customer'])) {
            if (!$broker->subscribed()) {
                $broker->status = 'inactive';
                $broker->saveOrFail();
                return $this->successMethod("Unsubscribed broker {$broker->name} #{$broker->id}.");
            }
            return $this->successMethod("Broker {$broker->name} #{$broker->id} is still subscribed.");
        }
        return $this->error("Broker not found {$payload['data']['object']['customer']}");
    }

    public function handlePaymentMethodAttached($payload)
    {
        // required to update default payment method when customer pays using Billing Portal
        if (!($broker = $this->getUserByStripeId($payload['data']['object']['customer']))) {
            return $this->error("Broker for stripe_id not found.");
        }
        if ($broker->hasDefaultPaymentMethod()) {
            return $this->successMethod(['message' => 'Broker already has default payment method.']);
        }
        $broker->updateDefaultPaymentMethod($payload['data']['object']['id']);
        return $this->successMethod();
    }

    /**
     * @param string|null $stripeId
     * @return \Laravel\Cashier\Billable|Broker
     */
    protected function getUserByStripeId($stripeId)
    {
        return parent::getUserByStripeId($stripeId);
    }

    /**
     * @param string|array $parameters
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    protected function successMethod($parameters = [])
    {
        return response()->json(is_string($parameters) ? ['message' => $parameters] : array_merge(['status' => 'processed'], $parameters));
    }

    protected function missingMethod($parameters = [])
    {
        return response()->json(array_merge(['status' => 'unhandled'], $parameters), 206);
    }

    /**
     * @param string|array $message
     * @param int          $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function error($message, $status = 422)
    {
        return response()->json(is_string($message) ? ['error' => $message] : $message, $status);
    }
}
