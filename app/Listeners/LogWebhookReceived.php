<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use Laravel\Cashier\Events\WebhookReceived;

class LogWebhookReceived
{

    public function handle(WebhookReceived $event)
    {
        Log::info("Received webhook {$event->payload['id']}", $event->payload);
    }
}
