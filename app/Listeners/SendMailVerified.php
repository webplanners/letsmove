<?php

namespace App\Listeners;

use App\Notifications\EmailVerified;
use Illuminate\Auth\Events\Verified;

class SendMailVerified
{

    public function handle(Verified $event)
    {
        $event->user->notify(new EmailVerified());
    }
}
