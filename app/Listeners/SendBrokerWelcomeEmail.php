<?php

namespace App\Listeners;

use App\Events\BrokerRegistered;
use App\Notifications\WelcomeBroker;

class SendBrokerWelcomeEmail
{

    public function handle(BrokerRegistered $event)
    {
        $event->broker->notify(new WelcomeBroker());
    }
}
