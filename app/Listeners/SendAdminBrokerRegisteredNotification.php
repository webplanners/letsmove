<?php

namespace App\Listeners;

use App\Events\BrokerRegistered;
use App\Models\Role;
use App\Notifications\NewBrokerRegistered;
use Illuminate\Support\Facades\Notification;

class SendAdminBrokerRegisteredNotification
{

    public function handle(BrokerRegistered $event)
    {
        Notification::send(Role::admin()->users, new NewBrokerRegistered($event->broker));
    }
}
