<?php

namespace App\Listeners;

use App\Events\PropertyApplicationReceived;
use App\Notifications\PropertyApplicationNotification;

class NotifyBrokerForPropertyApplication
{
    public function handle(PropertyApplicationReceived $event)
    {
        $event->pa->broker->notify(new PropertyApplicationNotification($event->pa));
    }
}
