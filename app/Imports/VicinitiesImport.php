<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VicinitiesImport implements ToModel, WithHeadingRow
{
    use Importable;

    public function model(array $row)
    {
        return rescue(function () use ($row) {
            return MunicipalitiesImport::municipality(array_pull($row, 'municipality_id'))->vicinities()->firstOrCreate(array_only($row, 'id'), $row);
        });
    }

}
