<?php

namespace App\Imports;

use App\Models\District;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DistrictsImport implements ToModel, WithHeadingRow
{
    use Importable;

    private static array $districts = [];

    /**
     * @param $id
     * @return District
     */
    public static function district($id)
    {
        return self::$districts[$id] ?? self::$districts[$id] = District::findOrFail($id);
    }

    public function model(array $row)
    {
        return self::$districts[$row['id']] = District::firstOrCreate(array_only($row, 'id'), $row);
    }
}
