<?php

namespace App\Imports;

use App\Models\District;
use App\Models\Municipality;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MunicipalitiesImport implements ToModel, WithHeadingRow
{
    use Importable;

    private static array $municipalities = [];

    /**
     * @param $id
     * @return Municipality
     */
    public static function municipality($id)
    {
        return self::$municipalities[$id] ?? self::$municipalities[$id] = Municipality::findOrFail($id);
    }

    public function model(array $row)
    {
        return rescue(function () use ($row) {
            return self::$municipalities[$row['id']] = DistrictsImport::district(array_pull($row, 'district_id'))->municipalities()->firstOrCreate(array_only($row, 'id'), $row);
        });
    }
}
