<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidHtml implements Rule
{
    public function passes($attribute, $value)
    {
        return $value != strip_tags($value);
    }

    public function message()
    {
        return 'Invalid HTML';
    }
}
