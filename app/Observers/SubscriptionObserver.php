<?php

namespace App\Observers;

use App\Models\Subscription;

class SubscriptionObserver
{
    public function updated(Subscription $s)
    {
        // if quantity/languages downgraded then remove extra languages
        if ($s->isDirty('quantity') &&
            $s->quantity < ($previous_quantity = $s->getOriginal('quantity')) &&
            $s->owner->languages->count() > $s->quantity
        ) {
            $s->owner->languages()->detach($s->owner->languages->reverse()->take($previous_quantity - $s->quantity));
        }
    }
}
