<?php

namespace App\Console\Commands\Import;

use App\Imports\MunicipalitiesImport;
use Illuminate\Console\Command;

class ImportMunicipalities extends Command
{
    protected $signature = 'import:municipalities {file}';
    protected $description = 'Import Municipalities';

    public function handle()
    {
        $this->output->title('Importing Municipalities...');
        (new MunicipalitiesImport())->withOutput($this->output)->import($this->argument('file'));
        $this->output->success('Completed');
        return 0;
    }
}
