<?php

namespace App\Console\Commands\Import;

use App\Imports\DistrictsImport;
use Illuminate\Console\Command;

class ImportDistricts extends Command
{
    protected $signature = 'import:districts {file}';
    protected $description = 'Import districts';

    public function handle()
    {
        $this->output->title('Importing Districts...');
        (new DistrictsImport)->withOutput($this->output)->import($this->argument('file'));
        $this->output->success('Completed');
        return 0;
    }
}
