<?php

namespace App\Console\Commands\Import;

use App\Imports\VicinitiesImport;
use Illuminate\Console\Command;

class ImportVicinities extends Command
{
    protected $signature = 'import:vicinities {file}';
    protected $description = 'Import Vicinities';

    public function handle()
    {
        $this->output->title('Importing Vicinities...');
        (new VicinitiesImport())->withOutput($this->output)->import($this->argument('file'));
        $this->output->success('Completed');
        return 0;
    }
}
