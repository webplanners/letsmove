<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (\Stripe\Exception\ApiErrorException $e) {
            Log::alert("Stripe exception: {$e->getMessage()}", ['requestId' => $e->getRequestId()]);
        });
        $this->reportable(function (\Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException $e) {
            if ($e->getPrevious() instanceof \Stripe\Exception\SignatureVerificationException) {
                Log::alert("Stripe invalid webhook secret: {$e->getMessage()}", ['exception' => telescope_exception()]);
            }
        });
        $this->renderable(function (\Spatie\MediaLibrary\MediaCollections\Exceptions\FileUnacceptableForCollection $e) {
            return response()->setStatusCode(412)
                ->json([
                    'message' => $e->getMessage(),
                ]);
        });
    }

}
