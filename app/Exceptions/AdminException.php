<?php

namespace App\Exceptions;

class AdminException extends BaseException
{
    public $status = 503; // 406 is not auto displayed by Nova
}
