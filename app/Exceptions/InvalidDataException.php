<?php

namespace App\Exceptions;

use Illuminate\Validation\ValidationException;

class InvalidDataException extends ValidationException
{
    public static function fromValidation(ValidationException $e)
    {
        $new = new static(null);
        $new->message = $e->message;
        $new->response = $e->response;
        $new->errorBag = $e->errorBag;
        $new->status = $e->status;
        $new->redirectTo = $e->redirectTo;
        return $new;
    }

    public function __construct($message)
    {
        parent::__construct(null);
        $this->message = $message;
    }

}
