<?php

namespace App\Checks;


use Spatie\ServerMonitor\CheckDefinitions\CheckDefinition;
use Symfony\Component\Process\Process;

class Horizon extends CheckDefinition
{
    public $command = 'php artisan horizon:status';

    public function __construct()
    {
        $this->command = 'php ' . base_path('artisan') . ' horizon:status';
    }

    public function resolve(Process $process)
    {
        if (trim($process->getOutput()) === 'Horizon is running.') {
            $this->check->succeed('is running');
            return;
        }
        $this->check->fail('is not running');
    }
}
