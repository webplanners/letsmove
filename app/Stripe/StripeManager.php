<?php
/**
 * StripeManager.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 2/12/20 12:32 μ.μ.
 */

namespace App\Stripe;


use App\Exceptions\InvalidDataException;
use App\Models\Broker;
use App\Models\User;
use Stripe\StripeObject;

class StripeManager
{
    public const SUBSCRIPTION_PLAN_PRODUCT = 'broker-listings';

    protected static \Stripe\StripeClient $stripe;

    /**
     * @return \Stripe\StripeClient
     */
    public static function client()
    {
        return static::$stripe;
    }

    public function __construct()
    {
        \Stripe\Stripe::setApiKey(config('cashier.secret'));
        static::$stripe = new \Stripe\StripeClient(config('cashier.secret'));
    }

    /**
     * @return \Illuminate\Support\Collection|StripeObject[]
     */
    public static function retrieveSubscriptionPlans()
    {
        $products = static::$stripe->products->all(['limit' => 1000, 'active' => true]);
        return collect($products->data)->where('metadata.product', static::SUBSCRIPTION_PLAN_PRODUCT);
    }

    /**
     * @param $productId
     * @return \Illuminate\Support\Collection|StripeObject[]
     */
    public static function retrievePricesForProduct($productId)
    {
        return collect(self::$stripe->prices->all(['limit' => 1000, 'active' => true, 'product' => $productId, 'type' => 'recurring', 'expand' => ['data.tiers']])->data);
    }

    /**
     * @param string           $stripe_price_id
     * @param int              $languages_quantity
     * @param User|Broker|null $user_broker
     * @return \Stripe\Checkout\Session
     * @throws \Stripe\Exception\ApiErrorException
     */
    public static function checkout($stripe_price_id, $languages_quantity, $user_broker = null)
    {
        // https://stripe.com/docs/billing/subscriptions/checkout
        // https://stripe.com/docs/api/checkout/sessions/object
        $broker = static::broker($user_broker);
        return \Stripe\Checkout\Session::create([
            'customer'             => $broker->broker_stripe_id,
            'success_url'          => route('nova.broker.current') . '?status=success&session_id={CHECKOUT_SESSION_ID}',
            'cancel_url'           => route('nova.broker.current') . '?status=failed',
//            'customer_email'       => $broker->email, // not with 'customer'
            'payment_method_types' => ['card'],
            'mode'                 => 'subscription',
            'line_items'           => [
                [
                    'price'    => $stripe_price_id,
                    'quantity' => $languages_quantity,
                ],
            ],
        ]);
    }

    /**
     * @param User|Broker $user_broker
     * @return Broker
     */
    public static function broker($user_broker = null)
    {
        if (empty($user_broker)) {
            $user_broker = request()->user();
        }
        if ($user_broker instanceof User) {
            $user_broker = throw_unless($user_broker->broker, InvalidDataException::class, __('User does not have a broker.'));
        }
        if (!$user_broker->hasStripeId()) {
            $user_broker->createAsStripeCustomer();
        }
        return $user_broker;
    }

}
