<?php

namespace App\Nova\Management;

use App\Nova\Actions\CopyPermissionsFromRole;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Role extends Resource
{
    public static $model = \App\Models\Role::class;
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $search = [
        'id', 'name',
    ];

    public function fields(Request $request)
    {
        $guardOptions = collect(config('auth.guards'))->mapWithKeys(function ($value, $key) {
            return [$key => $key];
        });
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')
                ->rules(['required', 'string', 'max:255'])
                ->creationRules('unique:' . config('permission.table_names.roles'))
                ->updateRules('unique:' . config('permission.table_names.roles') . ',name,{{resourceId}}'),
            Select::make(__('Guard Name'), 'guard_name')
                ->options($guardOptions->toArray())
                ->rules(['required', Rule::in($guardOptions)]),
            BelongsToMany::make(__('Permissions'), 'permissions', \App\Nova\Management\Permission::class)->searchable(),
            MorphToMany::make(__('Users'), 'users', \App\Nova\Management\User::class),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
//            $this->actionDownloadExcel(),//PAID
            new CopyPermissionsFromRole,
        ];
    }

    public function authorizedToView(Request $request)
    {
        return !$this->resource->is_super_admin && parent::authorizedToView($request);
    }

    public function authorizedToDelete(Request $request)
    {
        return !$this->resource->is_super_admin && parent::authorizedToDelete($request);
    }

    public function authorizedToUpdate(Request $request)
    {
        return !$this->resource->is_super_admin && parent::authorizedToUpdate($request);
    }

    public function authorizedToDetach(NovaRequest $request, $model, $relationship)
    {
        return !$this->resource->is_super_admin && parent::authorizedToDetach($request, $this->resource, $relationship);
    }

    public function authorizedToAttachAny(NovaRequest $request, $model)
    {
        return !$this->resource->is_super_admin && parent::authorizedToAttachAny($request, $model);
    }
}
