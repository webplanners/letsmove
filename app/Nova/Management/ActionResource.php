<?php

namespace App\Nova\Management;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\KeyValue;
use Laravel\Nova\Fields\MorphToActionTarget;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;
use Laravel\Nova\Panel;

class ActionResource extends Resource
{
    public static $model = \App\Models\Actions\ActionEvent::class;
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $subtitle = 'id';
    public static $search = [
        'id', 'name',
    ];

    public static function availableForNavigation(Request $request)
    {
        return static::$is_admin;
    }

    public function fields(Request $request)
    {
        // Taken from Laravel\Nova\Actions\ActionResource
        return [
            ID::make('ID', 'id'),
            Text::make(__('Action Name'), 'name', function ($value) {
                return __($value);
            }),
            Text::make(__('Action Initiated By'), function () {
                return $this->user->name ?? $this->user->email ?? __('Nova User');
            }),
            MorphToActionTarget::make(__('Action Target'), 'target'),
            Status::make(__('Action Status'), 'status', function ($value) {
                return __(ucfirst($value));
            })->loadingWhen([__('Waiting'), __('Running')])->failedWhen([__('Failed')]),
            $this->when(isset($this->original), function () {
                return KeyValue::make(__('Original'), 'original');
            }),
            $this->when(isset($this->changes), function () {
                return KeyValue::make(__('Changes'), 'changes');
            }),
            Textarea::make(__('Exception'), 'exception'),
            DateTime::make(__('Action Happened At'), 'created_at')->exceptOnForms(),
            $this->parametersPanel($request),
        ];
    }

    public function parametersPanel(Request $request)
    {
        return new Panel(__('Action Parameters'), collect($this->getFields())->map(function ($v, $k) {
            return Text::make(__(title_case(snake_case(studly_case($k), ' '))), function () use ($v) {
                return $v;
            })->hideFromIndex();
        })->all());
    }

    /**
     * @param NovaRequest $request
     * @return bool
     */
    public static function authorizedToViewAny(Request $request)
    {
        return Gate::check('viewAny', [static::$model, Nova::modelInstanceForKey($request->viaResource)]);
//        return Gate::check('viewAny', [static::$model, $request->findParentModel()]);
    }

    /**
     * @param NovaRequest $request
     * @return bool
     */
    public function authorizedToView(Request $request)
    {
        return Gate::check('view', [static::$model, Nova::modelInstanceForKey($request->viaResource)]);
//        return Gate::check('view',[static::$model, $request->findParentModel()]);
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->with('user');
    }

    public static function label()
    {
        return __('Actions');
    }

    public static function singularLabel()
    {
        return __('Action');
    }

    public static function uriKey()
    {
        return 'action-events';
    }
}
