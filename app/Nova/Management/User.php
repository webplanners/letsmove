<?php

namespace App\Nova\Management;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Jeffbeltran\SanctumTokens\SanctumTokens;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;

class User extends Resource
{
    public static $broker_related = true;
    public static $model = \App\Models\User::class;
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $search = ['id', 'name', 'email', 'phone',];

    public static function availableForNavigation(Request $request)
    {
        return static::$is_admin;
    }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Gravatar::make(__('Avatar'))->maxWidth(50),
            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),
            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),
            Password::make(__('Password'), 'password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:6')
                ->updateRules('nullable', 'string', 'min:6'),
            Text::make(__('Phone'), 'phone'),
            DateTime::make(__('Email Verified At'), 'email_verified_at')->sortable()->readonly(static::$broker_only),
            MorphToMany::make(__('Roles'), 'roles', \App\Nova\Management\Role::class), // hidden from role-permission
            MorphToMany::make(__('Permissions'), 'permissions', \App\Nova\Management\Permission::class), // hidden from role-permission
            BelongsTo::make(__('Broker'), 'broker', \App\Nova\Brokers\Broker::class)->readonly(static::$broker_only)->withoutTrashed(),
            SanctumTokens::make()->hideAbilities(),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
//            $this->actionDownloadExcel(),//PAID
        ];
    }

    public function subtitle()
    {
        return $this->email;
    }

}
