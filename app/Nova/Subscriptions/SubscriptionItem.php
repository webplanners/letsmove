<?php

namespace App\Nova\Subscriptions;

use App\Nova\Actions\SyncFromStripe;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;

class SubscriptionItem extends Resource
{
    public static $model = \App\Models\SubscriptionItem::class;
    public static $globallySearchable = false;
    public static $title = 'id';
    public static $search = [
        'id', 'stripe_id', 'stripe_plan',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Number::make(__('Quantity'), 'quantity'),
            Text::make(__('Stripe Id'), function () {
                return "$this->stripe_id";
            })->asHtml(),
            Text::make(__('Stripe Plan'), function () {
                return "$this->stripe_plan";
            })->asHtml(),
            BelongsTo::make(__('Subscription'), 'subscription', \App\Nova\Subscriptions\Subscription::class),
            $this->timestampablePanel($request),
        ];
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToRestore(Request $request)
    {
        return false;
    }

    public function actions(Request $request)
    {
        return [
            new SyncFromStripe(),
        ];
    }
}
