<?php

namespace App\Nova\Subscriptions;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\Text;

class Subscription extends Resource
{
    public static $broker_related = 'user_id';
    public static $model = \App\Models\Subscription::class;
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $search = [
        'id', 'name', 'stripe_id', 'stripe_plan', 'ends_at',
    ];

    public function fields(Request $request)
    {
        return array_flatten([
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')->readonly(),
            Number::make(__('Quantity'), 'quantity'),
            DateTime::make(__('Ends At'), 'ends_at'),
            Status::make(__('Status'), 'stripe_status')->failedWhen(['cancelled'])->loadingWhen(['incomplete']),
            $this->when(static::$is_admin, function () use ($request) {
                return [
                    BelongsTo::make(__('Broker'), 'broker', \App\Nova\Brokers\Broker::class)->withoutTrashed(),
                    HasMany::make(__('Subscription Items'), 'items', \App\Nova\Subscriptions\SubscriptionItem::class),
                    Text::make(__('Stripe Id'), function () {
                        return "<a href='https://dashboard.stripe.com/subscriptions/{$this->stripe_id}' target='_blank'>{$this->stripe_id}</a>";
                    })->asHtml()->hideFromIndex(),
                    Text::make(__('Stripe Plan'), function () {
                        return "<a href='https://dashboard.stripe.com/plans/{$this->stripe_plan}' target='_blank'>{$this->stripe_plan}</a>";
                    })->asHtml()->hideFromIndex(),
                ];
            }),
            $this->timestampablePanel($request),
        ]);
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToRestore(Request $request)
    {
        return false;
    }
}
