<?php

namespace App\Nova\Subscriptions;

use App\Nova\Actions\PurchaseSubscription;
use App\Nova\Actions\PurchaseSubscriptionInline;
use App\Nova\Actions\SyncSubscriptionPlans;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Panel;

class SubscriptionPlan extends Resource
{
    public static $model = \App\Models\SubscriptionPlan::class;
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $search = [
        'id', 'name', 'description', 'stripe_product_id', 'listings_limit',
    ];

    public function fields(Request $request)
    {
        return array_flatten(array_filter([
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name'),
            Text::make(__('Description'), 'description'),
            Number::make(__('Listings Limit'), 'listings_limit'),
            $this->monthlyPanel(),
            $this->yearlyPanel(),
            $this->when(static::$is_admin, function () use ($request) {
                return [
                    Text::make(__('Stripe Product Id'), function () {
                        return "<a href='https://dashboard.stripe.com/products/{$this->stripe_product_id}' target='_blank'>{$this->stripe_product_id}</a>";
                    })->asHtml(),
                    $this->groupedBlameablePanel($request),
                ];
            }),
        ]));
    }

    /**
     * @return Panel|null
     */
    private function monthlyPanel()
    {
        return empty($this->monthly_stripe_price)
            ? null
            : new Panel(__('Monthly Plan'), [
                $this->when(static::$is_admin, function () {
                    return Text::make(__('Stripe Price Id'), function () {
                        $id = $this->monthly_stripe_price['id'];
                        return "<a href='https://dashboard.stripe.com/prices/$id' target='_blank'>$id</a>";
                    })->asHtml();
                }),
                Number::make(__('Basic Monthly Price'), function () {
                    return $this->monthly_price;
                }),
                Number::make(__('Language Monthly Price'), function () {
                    return $this->monthly_language_price;
                }),
            ]);
    }

    /**
     * @return Panel|null
     */
    private function yearlyPanel()
    {
        return empty($this->yearly_stripe_price)
            ? null
            : new Panel(__('Yearly Plan'), [
                $this->when(static::$is_admin, function () {
                    return Text::make(__('Stripe Price Id'), function () {
                        $id = $this->yearly_stripe_price['id'];
                        return "<a href='https://dashboard.stripe.com/prices/$id' target='_blank'>$id</a>";
                    })->asHtml();
                }),
                Number::make(__('Basic Yearly Price'), function () {
                    return $this->yearly_price;
                }),
                Number::make(__('Language Yearly Price'), function () {
                    return $this->yearly_language_price;
                }),
            ]);
    }

    public function actions(Request $request)
    {
        return [
            new SyncSubscriptionPlans(),
            new PurchaseSubscriptionInline(),
        ];
    }
}
