<?php

namespace App\Nova\Subscriptions;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Language extends Resource
{
    public static $model = \App\Models\Language::class;
    public static $title = 'name';
    public static $subtitle = 'code';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'code', 'name',
    ];

    public static function availableForNavigation(Request $request)
    {
        return static::$is_admin;
    }

    public function fields(Request $request)
    {
        return array_flatten([
            $this->when(static::$is_admin, function () use ($request) {
                return ID::make(__('ID'), 'id')->sortable();
            }),
            Text::make(__('Name'), 'name')->required(),
            $this->when(static::$is_admin, function () use ($request) {
                return [
                    BelongsToMany::make(__('Brokers'), 'brokers', \App\Nova\Brokers\Broker::class),
                    $this->groupedBlameablePanel($request),
                    Slug::make(__('Code'), 'code')->from('name')->separator('_')
                        ->creationRules('unique:languages,code')
                        ->updateRules('unique:languages,code,{{resourceId}}')
                        ->required(),
                ];
            }),
        ]);
    }

    public function authorizedToAttachAny(NovaRequest $request, $model)
    {
        return false;
    }
}
