<?php

namespace App\Nova;

use App\Nova\Management\User;
use DigitalCloud\NovaResourceNotes\Fields\Notes;
use Dillingham\NovaGroupedField\Grouped;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;
use Laravel\Nova\Panel;
use Laravel\Nova\Resource as NovaResource;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

abstract class Resource extends NovaResource
{
    /**
     * Auto filled on every Request
     * @var boolean
     */
    public static $broker_only = null;
    /**
     * Auto filled on every Request
     * @var boolean
     */
    public static $is_broker = null;
    /**
     * Auto filled on every Request
     * @var boolean
     */
    public static $is_admin = null;

    /**
     * This is used to filter resources.
     * @var bool|string If string then match that DB column during indexQuery.
     */
    public static $broker_related;

    public static $group = 'Other';

    public static $subtitle;

    public function subtitle()
    {
        return static::$subtitle ? $this->{static::$subtitle} : null;
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder   $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return (static::$broker_related && static::$broker_only) ? $query->where(is_string(static::$broker_related) ? static::$broker_related : 'broker_id', $request->user()->broker_id) : $query;
    }

    /**
     * Build a Scout search query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Laravel\Scout\Builder                  $query
     * @return \Laravel\Scout\Builder
     */
    public static function scoutQuery(NovaRequest $request, $query)
    {
        return $query;
    }

    /**
     * Build a "detail" query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder   $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function detailQuery(NovaRequest $request, $query)
    {
        return parent::detailQuery($request, $query);
    }

    /**
     * Build a "relatable" query for the given resource.
     *
     * This query determines which instances of the model may be attached to other resources.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder   $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function relatableQuery(NovaRequest $request, $query)
    {
//        return static::$broker_related ? $query->where('broker_id', $request->user()->broker_id) : $query; //TODO check
        return parent::relatableQuery($request, $query);
    }


    public static function group()
    {
        return __(humanize(static::$group == 'Other' ? static::getDirPath() : static::$group));
    }

    public static function getDirPath()
    {
        return str_before(str_after(static::class, 'App\\Nova\\'), '\\');
    }

    protected function timestampablePanel(Request $request)
    {
        return (new Panel(__('Timestamps'), [
            DateTime::make(__('Created At'), 'created_at')->sortable()->exceptOnForms(),
            DateTime::make(__('Updated At'), 'updated_at')->sortable()->exceptOnForms(),
            DateTime::make(__('Deleted At'), 'deleted_at')->sortable()->exceptOnForms()->canSee(function () {
                return $this->deleted_at;
            }),
        ]));
    }

    protected function blameablePanel(Request $request)
    {
        return new Panel(__('Handlers'), [
            BelongsTo::make(__('Creator'), 'creator', User::class)->exceptOnForms()->hideFromIndex(),
            BelongsTo::make(__('Updater'), 'updater', User::class)->exceptOnForms()->hideFromIndex(),
            BelongsTo::make(__('Deleter'), 'deleter', User::class)->exceptOnForms()->hideFromIndex()->canSee(function () {
                return $this->deleted_at;
            }),
        ]);
    }

    /**
     * @param Request $request
     * @return Panel
     */
    protected function groupedBlameablePanel(Request $request)
    {
        $separator = '-';
        return new Panel(__('Timestamps') . " $separator " . __('Handlers'), [
            DateTime::make(__('Created At'), 'created_at')->sortable()->onlyOnIndex(),
//            DateTime::make(__('Updated At'), 'updated_at')->sortable()->onlyOnIndex(),
            DateTime::make(__('Deleted At'), 'deleted_at')->sortable()->onlyOnIndex()->canSee(function () {
                return $this->deleted_at;
            }),
            /***/
            Grouped::make(__('Created'))->fields([
                DateTime::make(__('Created At'), 'created_at'),
                BelongsTo::make(__('Creator'), 'creator', User::class),
            ])->hideFromIndex()->separator($separator),
            Grouped::make(__('Updated'))->fields([
                DateTime::make(__('Updated At'), 'updated_at'),
                BelongsTo::make(__('Updater'), 'updater', User::class),
            ])->hideFromIndex()->separator($separator),
            Grouped::make(__('Deleted'))->fields([
                DateTime::make(__('Deleted At'), 'deleted_at'),
                BelongsTo::make(__('Deleter'), 'deleter', User::class),
            ])->hideFromIndex()->separator($separator)->canSee(function () {
                return $this->deleted_at;
            }),
        ]);
    }

    protected function resourceLinkField($name, ?Model $model)
    {
        return Text::make((empty($model) ? $name : __(humanize($model))), function () use ($model) {
            if (empty($model))
                return '-';
            $resource = Nova::newResourceFromModel($model);
            return '<a href="' . url(config('nova.path') . '/resources/' . $resource::uriKey() . '/' . $model->getKey()) . '" class="no-underline font-bold dim text-primary">' . ($resource->title()) . '</a>';
        })->exceptOnForms()->asHtml();
    }

//    protected function linkField($name, $url, $value=null)
//    {
//        return Text::make($name, function () use ($url) {
//            $url = value($url);
//            if (empty($url))
//                return '-';
//            return '<a href="'.$url.'" class="no-underline font-bold hover:underline text-secondary">' . $url . '</a>';
//        })->exceptOnForms()->asHtml();
//    }

    /**
     * Get the displayble label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __(str_plural(static::resourceName()));
    }

    /**
     * Get the displayble singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __(static::resourceName());
    }

    public static function resourceName()
    {
        return Nova::humanize(class_basename(static::class));
    }

    /**
     * @return Notes
     */
    public function notesField()
    {
        return Notes::make(__('Notes'), 'notes')->hideFromIndex();
    }

    /**
     * @return DownloadExcel
     */
    public function actionDownloadExcel()
    {
        return (new DownloadExcel())->withHeadings();
    }

    public function authorizedToForceDelete(Request $request)
    {
        return false;
    }

    public function authorizedToView(Request $request)
    {
        return (static::$broker_related && static::$broker_only) ? $this->broker_id == $request->user()->broker_id : parent::authorizedToView($request);
    }

}
