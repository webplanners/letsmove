<?php

namespace App\Nova\Properties;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;

class PropertyApplication extends Resource
{
    public static $broker_related = true;
    public static $globallySearchable = false;
    public static $model = \App\Models\PropertyApplication::class;
    public static $title = 'id';
    public static $search = ['id', 'name', 'email', 'phone', 'message', 'created_at'];
    public static $with = ['property'];

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name'),
            Text::make(__('Email'), 'email'),
            Text::make(__('Phone'), 'phone'),
            Textarea::make(__('Message'), 'message')->alwaysShow(),
            BelongsTo::make(__('Property'), 'property', \App\Nova\Properties\Property::class)->exceptOnForms()->withoutTrashed(),
            BelongsTo::make(__('Broker'), 'broker', \App\Nova\Brokers\Broker::class)->exceptOnForms()->withoutTrashed(),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
//            $this->actionDownloadExcel(),//PAID
        ];
    }
}
