<?php

namespace App\Nova\Properties;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class PropertyCategory extends Resource
{
    public static $model = \App\Models\PropertyCategory::class;
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $search = [
        'id', 'name', 'code',
    ];

    public static function availableForNavigation(Request $request)
    {
        return static::$is_admin;
    }

    /**
     * @param NovaRequest $request
     * @return array
     */
    public function fields(Request $request)
    {
        return array_flatten([
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')->required(),
            Slug::make(__('Code'), 'code')->from('name')->separator('_')
                ->creationRules('unique:property_categories,code')
                ->updateRules('unique:property_categories,code,{{resourceId}}')
                ->required(),
            HasMany::make(__('Property Attributes'), 'property_attributes', \App\Nova\Properties\PropertyAttribute::class),
            HasMany::make(__('Properties'), 'properties', \App\Nova\Properties\Property::class),
            $this->when(static::$is_admin, function () use ($request) {
                return [
                    $this->groupedBlameablePanel($request),
                ];
            }),
        ]);
    }

}
