<?php

namespace App\Nova\Properties;

use App\Nova\Resource;
use App\Support\PropertyAttributes;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use NovaItemsField\Items;

class PropertyAttribute extends Resource
{
    public static $model = \App\Models\PropertyAttribute::class;
    public static $globallySearchable = false;
    public static $title = 'name';
    public static $search = [
        'id', 'code', 'name', 'group',
    ];

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Name'), 'name')->required()->sortable(),
            Slug::make(__('Code'), 'code')->from('name')->separator('_')
                ->creationRules('unique:property_attributes,code')
                ->updateRules('unique:property_attributes,code,{{resourceId}}')
                ->required(),
            BelongsTo::make(__('Property Category'), 'property_category', \App\Nova\Properties\PropertyCategory::class)->required()->sortable()->withoutTrashed(),
            Select::make(__('Type'), 'type')->displayUsingLabels()->options(PropertyAttributes::listTypes())->required()->sortable(),
            Text::make(__('Validation Rules'), 'validation'),
            Text::make(__('Group'), 'group')->sortable(),
            Items::make(__('Options'), 'options')->hideFromDetail()->draggable(),
            Items::make(__('Options'), 'translated_options')->onlyOnDetail(),
            $this->when(static::$is_admin, function () use ($request) {
                return $this->groupedBlameablePanel($request);
            }),
        ];
    }

}
