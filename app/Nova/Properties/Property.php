<?php

namespace App\Nova\Properties;

use App\Nova\Resource;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Eminiarts\Tabs\ActionsInTabs;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Heading;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;
use Media24si\NovaYoutubeField\Youtube;
use Orlyapps\NovaBelongsToDepend\NovaBelongsToDepend;

class Property extends Resource
{
    use ActionsInTabs;

//    use HasDependencies;

    public static $broker_related = true;
    public static $model = \App\Models\Property::class;
    public static $title = 'title';
    public static $search = ['id',];
    public static $with = ['district', 'municipality', 'vicinity', 'broker', 'property_category'];

    /**
     * @param NovaRequest $request
     * @return array
     */
    public function fields(Request $request)
    {
        return array_flatten([
            ID::make(__('ID'), 'id')->sortable(),
            BelongsTo::make(__('Property Category'), 'property_category', \App\Nova\Properties\PropertyCategory::class)->required()->withoutTrashed(),
            BelongsTo::make(__('Broker'), 'broker', \App\Nova\Brokers\Broker::class)->required()
                ->readonly(function () use ($request) {
                    // όταν είναι readonly εξαιρείται από το fill, άρα πρέπει να readonly μόνο στην προβολή φόρμας, αλλά όχι στην καταχώρηση
                    return static::$broker_only && $request->isCreateOrAttachRequest() && $request->isMethod('GET');
                })
                ->default($request->user()->broker_id) // χρειάζεται όταν ο μεσίτης κάνει create να συμπληρώνεται αυτόματα το ID του
                ->hideWhenUpdating(static::$broker_only)
                ->withoutTrashed(),
            Textarea::make(__('Description'), 'description')->rules('max:700')->rows(10),
            Youtube::make('YouTube', 'youtube'),
            Images::make(__('Photos'), 'photos')
                ->rules('max:' . config('property.photos.count')) // validation rules for the collection of images
//                ->withResponsiveImages() //PAID
//                ->showStatistics()
                ->singleImageRules(['image', 'max:' . config('property.photos.file_size')]),
            $this->locationPanel($request),
            HasMany::make(__('Property Applications'), 'property_applications', \App\Nova\Properties\PropertyApplication::class),
            $this->when($request->isCreateOrAttachRequest() || $request->isUpdateOrUpdateAttachedRequest(), function () {
                $i = 0;
                return \App\Models\PropertyCategory::all()->map(function (\App\Models\PropertyCategory $pc) use (&$i) {
                    return $pc->getPropertyAttributes()->groupBy('group')->map(function ($pas, $group) use (&$i, $pc) {
                        $heading = Heading::make("<h1 class='text-90 font-normal text-2xl mb-3'>" . humanize_attr($group) . "</h1>")->asHtml();
                        return Panel::make(str_pad('', ++$i, ' '), [
                            NovaDependencyContainer::make($pas->map->nova_field->prepend($heading)->toArray())
                                ->dependsOn('property_category.id', $pc->id),
                        ]);
                    })->toArray();
                });
            }),
            $this->when($request->isResourceDetailRequest(), function () {
                return $this->property_category->getNovaFields();
            }),
            $this->groupedBlameablePanel($request),
        ]);
    }

    /**
     * @param NovaRequest $request
     * @return Panel
     */
    private function locationPanel($request)
    {
        return new Panel(__('Property Location'), function () use ($request) {
            if ($request->isResourceDetailRequest() || $request->isResourceIndexRequest()) {
                return [
                    BelongsTo::make(__('District'), 'district', \App\Nova\Locations\District::class),
                    BelongsTo::make(__('Municipality'), 'municipality', \App\Nova\Locations\Municipality::class),
                    BelongsTo::make(__('Vicinity'), 'vicinity', \App\Nova\Locations\Vicinity::class),
                ];
            }
            return [
                NovaBelongsToDepend::make(__('District'), 'district', \App\Nova\Locations\District::class)
                    ->options(\App\Models\District::all()->append('name'))
                    ->required(),
                NovaBelongsToDepend::make(__('Municipality'), 'municipality', \App\Nova\Locations\Municipality::class)
                    ->optionsResolve(function ($district) {
                        return $district->municipalities->append('name');
                    })
                    ->dependsOn('district')
                    ->required(),
                NovaBelongsToDepend::make(__('Vicinity'), 'vicinity', \App\Nova\Locations\Vicinity::class)
                    ->optionsResolve(function ($municipality) {
                        return $municipality->vicinities->append('name');
                    })
                    ->dependsOn('municipality')
                    ->required(),
            ];
        });
    }

    public function actions(Request $request)
    {
        return [
//            $this->actionDownloadExcel(),//PAID
        ];
    }
}
