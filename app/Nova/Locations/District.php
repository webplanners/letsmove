<?php

namespace App\Nova\Locations;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasManyThrough;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class District extends Resource
{
    public static $model = \App\Models\District::class;
    public static $globallySearchable = true;
    public static $title = 'name';
    public static $search = ['id', 'name_el', 'name_en',];

    public function fields(Request $request)
    {
        return array_flatten([
            ID::make()->sortable(),
            Text::make(__('Greek Name'), 'name_el')->required()->sortable(),
            Text::make(__('English Name'), 'name_en')->required()->sortable(),
            HasMany::make(__('Municipalities'), 'municipalities', \App\Nova\Locations\Municipality::class),
            HasManyThrough::make(__('Vicinities'), 'vicinities', \App\Nova\Locations\Vicinity::class),
            HasMany::make(__('Properties'), 'properties', \App\Nova\Properties\Property::class),
            $this->when(static::$is_admin, function () use ($request) {
                return [
                    $this->groupedBlameablePanel($request),
                ];
            }),
        ]);
    }

    public function actions(Request $request)
    {
        return [
//            $this->actionDownloadExcel(),//PAID
        ];
    }

    public static function singularLabel()
    {
        return parent::singularLabel();
    }

    public static function label()
    {
        return '1. ' . parent::label();
    }
}
