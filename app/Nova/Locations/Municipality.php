<?php

namespace App\Nova\Locations;

use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;

class Municipality extends Resource
{
    public static $model = \App\Models\Municipality::class;
    public static $globallySearchable = true;
    public static $title = 'name';
    public static $search = ['id', 'name_el', 'name_en',];
    public static $with = ['district'];

    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Greek Name'), 'name_el')->required(),
            Text::make(__('English Name'), 'name_en')->required(),
            BelongsTo::make(__('District'), 'district', \App\Nova\Locations\District::class)->required()->searchable()->showCreateRelationButton()->withoutTrashed(),
            HasMany::make(__('Vicinities'), 'vicinities', \App\Nova\Locations\Vicinity::class),
            HasMany::make(__('Properties'), 'properties', \App\Nova\Properties\Property::class),
            $this->when(static::$is_admin, function () use ($request) {
                return $this->groupedBlameablePanel($request);
            }),
        ];
    }

    public function actions(Request $request)
    {
        return [
//            $this->actionDownloadExcel(),//PAID
        ];
    }

    public static function singularLabel()
    {
        return parent::singularLabel();
    }

    public static function label()
    {
        return '2. ' . parent::label();
    }

}
