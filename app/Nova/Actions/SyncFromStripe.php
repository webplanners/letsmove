<?php

namespace App\Nova\Actions;

use App\Models\Broker;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Cashier\Exceptions\CustomerAlreadyCreated;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class SyncFromStripe extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public function __construct()
    {
        $this->seeCallback = function (Request $request) {
            return static::$is_admin;
        };
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields             $fields
     * @param \Illuminate\Support\Collection|Subscription[] $subscriptions
     * @return mixed
     */
    public function handleForSubscriptions(ActionFields $fields, Collection $subscriptions)
    {
        $synced = collect();
        foreach ($subscriptions as $subscription) {
            try {
                $subscription->syncFromStripe();
                $synced->push($subscription);
            } catch (\Exception $e) {
                report($e);
            }
        }
        if ($subscriptions->count() == 1) {
            return $synced->count()
                ? Action::message(__('Synced Successfully'))
                : Action::danger(__('Failed to sync'));
        }
        return $synced->count()
            ? Action::message(__('Synced :count resources', ['count' => $synced->count()]))
            : Action::danger(__('Failed to sync resources'));
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields       $fields
     * @param \Illuminate\Support\Collection|Broker[] $brokers
     * @return mixed
     */
    public function handleForBrokers(ActionFields $fields, Collection $brokers)
    {
        $synced = collect();
        foreach ($brokers as $broker) {
            try {
                $broker->syncStripeIdFromStripe();
            } catch (CustomerAlreadyCreated $e) {
            }
            try {
                $broker->syncSubscriptionsFromStripe();
                $synced->push($broker);
            } catch (\Exception $e) {
                report($e);
            }
        }
        if ($brokers->count() == 1) {
            return $synced->count()
                ? Action::message(__('Synced Successfully'))
                : Action::danger(__('Failed to sync'));
        }
        return $synced->count()
            ? Action::message(__('Synced :count resources', ['count' => $synced->count()]))
            : Action::danger(__('Failed to sync resources'));
    }

    public function fields()
    {
        return [];
    }
}
