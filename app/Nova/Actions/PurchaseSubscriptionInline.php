<?php

namespace App\Nova\Actions;

use App\Models\Language;
use App\Models\SubscriptionPlan;
use App\Stripe\StripeManager;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Number;

class PurchaseSubscriptionInline extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public function __construct()
    {
        $this->onlyOnTableRow();
        $this->showOnDetail();
        $this->confirmButtonText = __('Summary');
        $this->seeCallback = function () {
            return static::$broker_only;
        };
        $this->runCallback = function () {
            return true;
        };
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields                 $fields
     * @param \Illuminate\Support\Collection|SubscriptionPlan[] $models
     * @return mixed
     */
    public function handleForSubscriptionPlans(ActionFields $fields, Collection $models)
    {
        $broker = Auth::user()->broker;
        if (empty($broker)) {
            return Action::danger(__('You are not a broker yet.'));
        }
        if ($broker->subscribed()) {
            return Action::danger(__('You already have subscription. For changes go to billing portal.'));
        }
        $extra_languages = $fields->get('extra_languages');
        $quantity = 1 + $extra_languages;
        /** @var SubscriptionPlan $subscriptionPlan */
        $subscriptionPlan = $this->singleModel($models);
        $subscriptionPlan = $subscriptionPlan->yearly_stripe_price['id'];
        $checkout = app(StripeManager::class)->checkout($subscriptionPlan, $quantity, $broker);
        return Action::push('/stripe/checkout', ['id' => $checkout->id]);
    }

    public function fields()
    {
        return [
            Number::make(__('Extra Languages'), 'extra_languages')
                ->min(0)->default(0)->max($max = Language::count() - Language::defaults()->count())->step(1)
                ->required()->rules('min:0', "max:$max")
                ->help(__('How many extra languages do you want to purchase other than the default ones.') . '<br>' . __('Default languages are') . ' ' . Language::defaults()->pluck('localized_name')->implode(', ')),
        ];
    }

    public function name()
    {
        return __('Purchase Subscription');
    }
}
