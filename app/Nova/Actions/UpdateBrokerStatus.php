<?php

namespace App\Nova\Actions;

use App\Models\Broker;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Select;

class UpdateBrokerStatus extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public function __construct()
    {
        $this->seeCallback = function (Request $request) {
            return static::$is_admin;
        };
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields       $fields
     * @param \Illuminate\Support\Collection|Broker[] $models
     * @return mixed
     */
    public function handleForBrokers(ActionFields $fields, Collection $models)
    {
        $status = $fields->get('status');
        foreach ($models as $broker) {
            $broker->status = $status;
            $broker->saveOrFail();
        }
    }

    public function fields()
    {
        return [
            Select::make(__('Status'), 'status')->options(Broker::listStatuses())->required(),
        ];
    }
}
