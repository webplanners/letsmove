<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class SyncSubscriptionPlans extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public $standalone = true;
    public $withoutActionEvents = true;

    public function __construct()
    {
        $this->onlyOnIndex();
        $this->seeCallback = function (Request $request) {
            return static::$is_admin;
        };
    }

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection    $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        Artisan::call('stripe:sync:subscription-plans');
        return Action::message(__('Synchronization completed'));
    }

    public function fields()
    {
        return [];
    }
}
