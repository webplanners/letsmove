<?php

namespace App\Nova\Actions;

use App\Models\Broker;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Cashier\Exceptions\InvalidCustomer;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class GoToBillingPortal extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public $withoutActionEvents = true;
    public $withoutConfirmation = true;
    public $onlyOnDetail = true;

    /**
     * @param \Laravel\Nova\Fields\ActionFields       $fields
     * @param \Illuminate\Support\Collection|Broker[] $models
     * @return mixed
     */
    public function handleForBrokers(ActionFields $fields, Collection $models)
    {
        /** @var Broker $broker */
        $broker = $this->singleModel($models);
        try {
            if (!$broker->hasStripeId()) {
                $broker->createAsStripeCustomer();
            }
            return Action::openInNewTab($broker->billingPortalUrl(route('nova.broker.current')));
        } catch (InvalidCustomer $e) {
            return Action::danger(__('Broker is not a customer yet.'));
        }
    }

    public function fields()
    {
        return [];
    }
}
