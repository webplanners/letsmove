<?php

namespace App\Nova\Actions;

use App\Models\Broker;
use App\Models\Language;
use App\Models\SubscriptionPlan;
use App\Stripe\StripeManager;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;

class PurchaseSubscription extends BaseAction
{
    use InteractsWithQueue, Queueable;

    public $onlyOnDetail = true;

    public function __construct()
    {
        $this->confirmButtonText = __('Summary');
    }

    /**
     * @param \Laravel\Nova\Fields\ActionFields       $fields
     * @param \Illuminate\Support\Collection|Broker[] $models
     * @return mixed
     */
    public function handleForBrokers(ActionFields $fields, Collection $models)
    {
        /** @var Broker $broker */
        $broker = $this->singleModel($models);
        $price_id = $fields->get('stripe_price_id');
        $extra_languages = $fields->get('extra_languages');
        $quantity = 1 + $extra_languages;
        if ($broker->subscribed()) {
            return Action::danger(__('You already have subscription. For changes go to billing portal.'));
        }
        $checkout = app(StripeManager::class)->checkout($price_id, $quantity, $broker);
        return Action::push('/stripe/checkout', ['id' => $checkout->id]);
    }

    public function fields()
    {
        return [
            Select::make(__('Subscription Plan'), 'stripe_price_id')
                ->required()
                ->options(SubscriptionPlan::all()
                    ->mapWithKeys(function (SubscriptionPlan $s) {
                        $plans = [];
                        if ($s->yearly_stripe_price) {
                            $plans[$s->yearly_stripe_price['id']] = ['label' => $s->yearly_display_name, 'group' => $s->display_name];
                        }
                        if ($s->monthly_stripe_price) {
                            $plans[$s->monthly_stripe_price['id']] = ['label' => $s->monthly_display_name, 'group' => $s->display_name];
                        }
                        return $plans;
                    })
                ),
            Number::make(__('Extra Languages'), 'extra_languages')
                ->min(0)->default(0)->max(Language::count() - Language::defaults()->count())->step(1)
                ->required()->rules('min:0')
                ->help(__('How many extra languages do you want to purchase other than the default ones.') . '<br>' . __('Default languages are') . ' ' . Language::defaults()->pluck('localized_name')->implode(', ')),
        ];
    }

}
