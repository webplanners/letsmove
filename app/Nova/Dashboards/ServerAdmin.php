<?php

namespace App\Nova\Dashboards;

use Laravel\Nova\Dashboard;

class ServerAdmin extends Dashboard
{
    public function __construct($component = null)
    {
        parent::__construct($component);
        $this->canSeeWhen('server-admin');
    }

    public function cards()
    {
        return [
            \Llaski\NovaServerMetrics\Card::make(),
        ];
    }

    public static function label()
    {
        return 'Server Admin';
    }

    public static function uriKey()
    {
        return 'server-admin';
    }
}
