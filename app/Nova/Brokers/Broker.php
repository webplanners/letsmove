<?php

namespace App\Nova\Brokers;

use App\Nova\Actions\GoToBillingPortal;
use App\Nova\Actions\PurchaseSubscription;
use App\Nova\Actions\SyncFromStripe;
use App\Nova\Actions\UpdateBrokerStatus;
use App\Nova\Resource;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;
use Sixlive\TextCopy\TextCopy;

class Broker extends Resource
{
    public static $broker_related = 'id';
    public static $model = \App\Models\Broker::class;
    public static $title = 'name';
    public static $globallySearchable = false;
    public static $search = [
        'id', 'stripe_id', 'name', 'address', 'phone', 'phone_2', 'vin', 'commercial_registration_number', 'email', 'website', 'instagram_username', 'facebook_username',
    ];

    public static function availableForNavigation(Request $request)
    {
        return static::$is_admin;
    }

    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make(__('Name'), 'name')->required()->rules('max:50'),
            Status::make(__('Status'), 'status')
                ->loadingWhen(array_map('humanize_attr', ['unverified']))
                ->failedWhen(array_map('humanize_attr', ['banned', 'inactive']))
                ->displayUsing('humanize_attr'),
            Image::make(__('Logo'), 'logo')->maxWidth(1000)->rules('max:' . config('broker.logo.size'))->disk(config('broker.logo.disk'))->path(config('broker.logo.path')),
            Textarea::make(__('Description'), 'description')->rules('max:65535'),
            new Panel(__('Tax Info'), [
                Text::make(__('Address'), 'address')->required()->rules('max:255')
                    ->displayUsing(function ($address) {
                        $address_param = urlencode($address);
                        return "<a href='https://www.google.com/maps/place/$address_param' target='_blank' class='no-underline font-medium text-green'>$address</a>";
                    })->asHtml(),
                Text::make(__('VIN'), 'vin')->required()->rules('digits:9')
                    ->displayUsing(function ($id) {
                        $id_param = urlencode($id);
                        return "<a href='https://www.businessregistry.gr/publicity/index?afm=$id_param' target='_blank' class='no-underline font-medium text-secondary'>$id</a>";
                    })->asHtml(),
                Text::make(__('Commercial Registration Number'), 'commercial_registration_number')->rules('nullable', 'digits:12')
                    ->displayUsing(function ($id) {
                        $id_param = urlencode($id);
                        return "<a href='https://www.businessregistry.gr/publicity/show/$id_param' target='_blank' class='no-underline font-medium text-secondary'>$id</a>";
                    })->asHtml(),
            ]),
            new Panel(__('Contact Info'), [
                Text::make(__('Phone'), 'phone')->required()->rules('max:13')
                    ->displayUsing(function ($phone) {
                        return "<a href='tel:$phone' target='_blank' class='no-underline font-medium'>$phone</a>";
                    })->asHtml(),
                Text::make(__('Phone') . ' 2', 'phone_2')->rules('max:13')
                    ->displayUsing(function ($phone) {
                        return "<a href='tel:$phone' target='_blank' class='no-underline font-medium '>$phone</a>";
                    })->asHtml(),
                Text::make(__('Email'), 'email')->rules('email', 'max:50')->required()
                    ->withMeta(['extraAttributes' => ['type' => 'email']])->displayUsing(function ($email) {
                        return "<a href='mailto:$email' target='_blank' class='no-underline font-medium text-blue-light'>$email</a>";
                    })->asHtml(),
            ]),
            new Panel(__('Webpages'), [
                Text::make(__('Website'), 'website')->rules('max:40')
                    ->displayUsing(function ($website) {
                        $website = e($website);
                        return "<a href='$website' target='_blank' class='no-underline font-medium text-secondary'>$website</a>";
                    })->asHtml(),
                Text::make(__('Facebook'), 'facebook_username')->rules('max:40')->displayUsing(function ($username) {
                    return "<a href='{$this->facebook_url}' target='_blank' class='no-underline font-medium text-secondary'>$username</a>";
                })->asHtml(),
                Text::make(__('Instagram'), 'instagram_username')->rules('max:40')->displayUsing(function ($username) {
                    return "<a href='{$this->instagram_url}' target='_blank' class='no-underline font-medium text-secondary'>$username</a>";
                })->asHtml(),
            ]),
            Select::make(__('Broker Type'), 'type')->options(\App\Models\Broker::listTypes())->displayUsingLabels()->required()
                ->showOnUpdating(static::$is_admin),
            HasMany::make(__('Property Applications'), 'property_applications', \App\Nova\Properties\PropertyApplication::class),
            HasMany::make(__('Properties'), 'properties', \App\Nova\Properties\Property::class),
            BelongsToMany::make(__('Languages'), 'languages', \App\Nova\Subscriptions\Language::class),
//            HasMany::make(__('Users'), 'users', \App\Nova\Management\User::class), //PAID
            HasMany::make(__('Subscriptions'), 'subscriptions', \App\Nova\Subscriptions\Subscription::class),
            TextCopy::make(__('Stripe ID'), 'stripe_id')->onlyOnDetail()->hideFromDetail(static::$broker_only),
            Stack::make(__('Card'), [
                Line::make(__('Card brand'), 'card_brand')->asHeading(),
                Line::make(__('Card Last Four'), 'card_last_four')->asSubTitle(),
            ]),
            $this->groupedBlameablePanel($request),
        ];
    }

    public function actions(Request $request)
    {
        return [
//            $this->actionDownloadExcel(),//PAID
            new UpdateBrokerStatus(),
            new SyncFromStripe(),
            new PurchaseSubscription(),
            new GoToBillingPortal(),
        ];
    }

}
