<?php

return [
    'photos' => [
        'disk'      => 'public',
        'path'      => 'properties',
        'count'     => 15,
        'file_size' => 5 * 1024, // kB
    ],
];
