<?php
/**
 * broker.php
 * User: Theofanis V. (vardtheo@gmail.com)
 * Date: 4/12/20 1:11 π.μ.
 */

return [
    'logo' => [
        'disk' => 'public',
        'path' => 'brokers/logo',
        'size' => 512, //kb
    ],
];
